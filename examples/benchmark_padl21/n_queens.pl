% N-queens program.
nqueens(N, Q) :-
    nqueens_(N, N, [], Q).

% Pick queens one at a time.
nqueens_(X, N, Qi, Qo) :-
    X > 0,
    pickqueen(X, Y, N),
    X1 is X - 1,
    nqueens_(X1, N, [queen(X, Y) | Qi], Qo).
nqueens_(0, _, Q, Q).


% pick a queen for row X.
pickqueen(X, Y, Y) :-
    Y > 0,
    queen(X, Y).
pickqueen(X, Y, N) :-
    N > 1,
    N1 is N - 1,
    pickqueen(X, Y, N1).

queen(X, Y) :- not neg_q(X, Y).
neg_q(X, Y) :- not queen(X, Y).

% Test
:- queen(I,J1), queen(I,J2), J1 \= J2.
:- queen(I1,J), queen(I2,J), I1 \= I2.
:- queen(I,J), queen(II,JJ), I \= II, J \= JJ, T1 is I+J, T2 is II+JJ, T1 = T2.
:- queen(I,J), queen(II,JJ), I \= II, J \= JJ, T1 is I-J, T2 is II-JJ, T1 = T2.


%% dcc(queen(I,J1)) :- queen(I,J2), J1 \= J2.
%% dcc(queen(I,J1)) :- queen(I,J2), J1 \= J2.
%% dcc(queen(I1,J)) :- queen(I2,J), I1 \= I2.
%% dcc(queen(I1,J)) :- queen(I2,J), I1 \= I2.
%% dcc(queen(I,J)) :- queen(II,JJ), I \= II, J \= JJ, T1 is I+J, T2 is II+JJ, T1 = T2.
%% dcc(queen(I,J)) :- queen(II,JJ), I \= II, J \= JJ, T1 is I+J, T2 is II+JJ, T1 = T2.
%% dcc(queen(I,J)) :- queen(II,JJ), I \= II, J \= JJ, T1 is I-J, T2 is II-JJ, T1 = T2.
%% dcc(queen(I,J)) :- queen(II,JJ), I \= II, J \= JJ, T1 is I-J, T2 is II-JJ, T1 = T2.

#show queen/2.
