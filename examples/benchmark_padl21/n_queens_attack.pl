
nqueens(N, Q) :-
    nqueens_(N, N, [], Q).

% Pick queens one at a time and test against all previous queens.
nqueens_(X, N, Qi, Qo) :-
    X > 0,
    pickqueen(X, Y, N),
    not attack(X, Y, Qi),
    X1 is X - 1,
    nqueens_(X1, N, [q(X, Y) | Qi], Qo).
nqueens_(0, _, Q, Q).

% pick a queen for row X.
pickqueen(X, Y, Y) :-
    Y > 0,
    q(X, Y).
pickqueen(X, Y, N) :-
    N > 1,
    N1 is N - 1,
    pickqueen(X, Y, N1).

% check if a queen can attack any previously selected queen.
attack(X, _, [q(X, _) | _]). % same row
attack(_, Y, [q(_, Y) | _]). % same col
attack(X, Y, [q(X2, Y2) | _]) :- % same diagonal 1
    T1 is X + Y,
    T2 is X2 + Y2,
    T1 = T2.
attack(X, Y, [q(X2, Y2) | _]) :- % same diagonal 2
    T1 is X - Y,
    T2 is X2 - Y2,
    T1 = T2.
attack(X, Y, [_ | T]) :-
    attack(X, Y, T).

q(X, Y) :- not neg_q(X, Y).
neg_q(X, Y) :- not q(X, Y).

#show q/2.