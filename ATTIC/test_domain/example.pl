% All domains here are single arity, but that does
% not need to be the case.
#domain vegi/1.
vegi(lettuce).
vegi(onion).
vegi(tomato).
vegi(tofu).

#domain meat/1.
meat(ham).
meat(beef).
meat(chicken).

#domain ingredients/1.
ingredients(X) :- meat(X).
ingredients(X) :- vegi(X).

#domain dish/1.
dish(hamburger).
dish(green_salad).
dish(chicken_salad).
dish(tofuburger).

in(hamburger, beef).
in(hamburger, lettuce).
in(hamburger, onion).
in(hamburger, tomato).
in(green_salad, lettuce).
in(green_salad, onion).
in(green_salad, tomato).
in(chicken_salad, chicken).
in(chicken_salad, lettuce).
in(chicken_salad, onion).
in(chicken_salad, tomato).
in(tofuburger, tofu).
in(tofuburger, lettuce).
in(tofuburger, onion).
in(tofuburger, tomato).

-vegitarian(D) :- dish(D), meat(I), in(D,I).
vegitarian(D) :- not -vegitarian(D).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dual Rules
% 
% Domain duals are generated as normal. This is the codomain
%   domain predicates are not affected

% not ingredients(X) :- not meat(X), not vegi(X).

% not -vegitarian(D) :- forall(I, n_-vegitarian1(D,I)).
% n_-vegitarian1(D,I) :- dish(D),meat(I), not in(D,I). %% We may also negate the forall domain, but ignore the answer when using sasp style duals

% not vegitarian(D) would be as is is now in scasp

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Queries

?- meat(lettuce).
% Result: Exit with error. Domain not defined for meat(lettuce).

%% This behaviour is not consistent with the forall/2 :(
?- vegitarian(D).
% Result1: D=green_salad
% Result1: D=tofuburger

% ?- vegitarian(hamburger).
% Result: No models.

% ?- vegitarian(tofu).
% Result: Exit with error. Domain not defined for dish(tofu).

?- vegitarian(tofuburger).