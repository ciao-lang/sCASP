


p(X) :- r(X), var(X),
    display(X), display(' es una variable').
p(X) :- r(X), not var(X),
    display(X), display(' no es una variable').

r(X).
%r(a).

?- p(X).
?- p(a).

    