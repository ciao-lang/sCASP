%            ┌───┐
%            │fl │
%            └─┬─┘
%       ┌──────┴─────┬───┐
%       │    ┌───┐ ┌─▼─┐ │     ┌───┐
%       │    │ a │ │*I*│ │   ┌─┤ r ├─┐                  Strata                  Lowered facts
%       │    └─┬─┘ └───┘ │   │ └───┘ │
%     ┌─▼─┐  ┌─▼─┐ ┌───┐ │ ┌─▼─┐   ┌─▼─┐            r | e | a | fl              r | e | a | fl
%   ┌─┤ c ◄──► b │ │ e │ │ │ q ◄─┐ │ o │         --------------------        --------------------
%   │ └─┬─┘  └─┬─┘ └─┬─┘ │ └─┬─┘ │ └─┬─┘            c b | o | *I*               c  b  |   o
%   │ ┌─▼─┐  ┌─▼─┐   │   │ ┌─▼─┐ │ ┌─▼─┐         --------------------        --------------------
%   │ │ g │  │ d ◄───┘───┘ │ s │ └─► p │              p q | d | g               p q  | d |  g
%   │ └─┬─┘  └─┬─┘         └─┬─┘   └─┬─┘         --------------------        --------------------
%   │ ┌─▼─┐  ┌─▼─┐         ┌─▼─┐     │               *H* | *F* | s                      s
%   │ │*H*│  │*F*│         │ t ◄─────┘           --------------------        --------------------
%   │ └───┘  └─▲─┘         └───┘                           t                  t | *I* | *H* | *F*
%   └──────────┘   

fl :- i, c, d.
a :- b.
b :- c, d.
c :- b, g, f.
d :- f.
e :- d, c.
f.
g :- h.
h.
i.
o :- p.
p :- t.
p :- q.
q :- p, s.
r :- o.
r :- q.
s :- t.
t.