

p:-
    %% -v5
    !display(entry_point), !nl,
    %% -v6
    !spy,
    a.

a.
a :-
    b(X),
    !pause([X]),
    c(Y),
    %% -v6 !pause(Vars): b print bindings of Vars...
    %%                   t print stack tree
    %%                   m print model (support #show)
    !pause([X,Y]). 

%% uncomment to restrict the model to b/1 and a/0 (remove p)...
#show b/1, a/0.

b(1).
b(2).

c(a).
c(b).


?- p.
%?- not p.