

p :- not neg_p.
neg_p :- not p.
q :- p.

s.

:- p, q.
:- s.

?- not p.
?- q.
?- s.