% fact for each vertex(N).
vertex(a).
vertex(b).
vertex(c).
vertex(d).
vertex(e).
vertex(f).

% fact for each edge edge(U, V).
edge(a, c).
edge(c, b).
edge(c, e).
edge(a, e).
edge(b, e).

edge(a, b).
edge(b, c).
edge(c, d).
edge(d, e).
edge(e, f).
edge(f, a).


reachable(V) :- chosen(V, a).
reachable(V) :- chosen(V, U), reachable(U).


% Choose exactly one edge from each vertex.
chosen(U, V) :-
    edge(U, V), not other(U, V).
other(U, V) :-
    edge(U, V), not chosen(U, V).

% Every vertex must be reachable.
:- vertex(U), not reachable(U).
% You cannot choose two edges to the same vertex
:- chosen(U, W), U \= V, chosen(V, W).
:- chosen(W, U), U \= V, chosen(W, V).




%%% To use preliminary-DCC introduce the dcc/1 rules corresponding to
%%% the NMR-check (the translation will be prefomed by the compiler in
%%% the final version).
%% dcc(chosen(U,W)) :- U \= V, chosen(V,W).
%% %dcc(chosen(V,W)) :- chosen(U,W), U \= V.
%% dcc(chosen(W,U)) :- U \= V, chosen(W,V).
%dcc(chosen(W,V)) :- chosen(W,U), U \= V.

?- reachable(a).

#show chosen/2.