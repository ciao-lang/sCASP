p:-not q.
q:-t,not u.
q:-not r.
r:-not s.
s:-q,not p.

% scasp steps.pl --forget='p,q'

% Result:
% r :- not s.
% s :- t, not u, not neg_3.
% s :- not r, not neg_3.
% neg_1 :- t, not u.
% neg_1 :- not r.
% neg_3 :- not neg_1.