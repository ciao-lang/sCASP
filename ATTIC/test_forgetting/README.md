# Testing files for flag --forget
## Syntax
`
scasp <file> --forget='<predicates>'/<SCASP_exec>
`

For example, to print the results of forgetting the predicates `p` and `q` from the program `test1` and transforming any double negations within the program, the command would be:

`
scasp test1.pl --forget='p,q'/1
`

If transforming double negations is unnecessary, the command would be:

`
scasp test1.pl --forget='p,q'/0
`