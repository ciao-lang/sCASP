p  :- not not p.
q :- p.
r :- not p.

% scasp 5-2.pl --forget='p'/0

% Result:
% q :- not neg_1.
% r :- not not neg_1.
% neg_1 :- not not neg_1.
