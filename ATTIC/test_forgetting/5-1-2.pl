a :- p.
b :- q.
p :- not q.
q :- r.
r :- not p.

% scasp 5-1-2.pl --forget='p,q'/0

% Result:
% a :- not not neg_2.
% b :- r.
% r :- not not neg_1.
% neg_1 :- r.
% neg_2 :- not r.