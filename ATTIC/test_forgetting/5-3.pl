a :- p, q.
q :- not p.
p :- not not p.

% scasp 5-3.pl --forget='p,q'/0

% Result:
% a :- not neg_1, not not neg_1.
% neg_1 :- not not neg_1.
