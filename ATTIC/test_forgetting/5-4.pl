q :- not not q, b.
a :- q.
c :- not q.

% scasp 5-4.pl --forget='q'/0

% Result:
% a :- not neg_1, b.
% c :- not not neg_1.
% neg_1 :- not not neg_1.
% neg_1 :- not b.
