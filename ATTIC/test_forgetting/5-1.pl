a :- p.
b :- q.
p :- not q.
q :- not p.

% scasp 5-1.pl --forget='p,q'/0

% Result:
% a :- not not neg_2.
% b :- not not neg_1.
% neg_1 :- not not neg_1.
% neg_2 :- not neg_1.
