r :- p.
r :- q.
p.

% scasp facts.pl --forget='q'

% Result:
% r :- p.
% p.

% scasp facts.pl --forget='p'

% Result:
% r.
% r :- q.