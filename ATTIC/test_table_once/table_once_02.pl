

# table_once s/1.

r :- p, s(1), fail.
r :- p, s(1), a(1).
r :- p, s(1), a(1).

r :- q, s(1), a(1).

p :- not q.
q :- not p.

s(1) :- a(X).

a(1).
a(2).

?- r.
