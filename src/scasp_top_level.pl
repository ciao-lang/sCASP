%:- package(scasp_top_level).

%% :- if(defined('SHELL')).
%% :- else.
%% :- export([
%%     main/2,
%%     binding/2
%% ]).
%% :- endif.


:- use_module(scasp).

:- op(700, fx, [not, (?=), (??), (?)]).

main(Args, [query(Query), answer(Answer), bindings(PVars, Bindings), model(Model)]) :-
    
    scasp_exec(Args, [Query, Answer, PVars, Bindings, Model]).


binding(Args, Bindings) :-
    scasp_exec(Args, [_Query, _Answer, _PVars, Bindings, _Model]).
    