:- module(clp_stratification, _).


%% ------------------------------------------------------------- %%
:- use_package(assertions).
:- doc(filetype, module).

:- doc(module, "

This module implements the core of stratification using CLP.").

%% ------------------------------------------------------------- %%
:- use_package(clpfd).
:- use_module('../../common_dependencies/auxiliary_functions/auxiliary.pl', [
    merge_lists/3,repeated_elements/3,present_in_both/3,
    delete_all_occurrences/3, select_non_repeated_elements_beta/3, flatten_list/2,
    last_element/2]).
:- use_module('../../common_dependencies/graph/graph', [
    find_all_dependencies/4, find_all_direct_dependencies/4
    ]).
:- use_module('./stratification_aux').


%% ------------------------------------------------------------- %%
%%                          Main calls                           %%


%!  create_strata(+Codependencies:list, +Modules:list, +Graph:list, -Strata:list)
%       Given a list of codependent predicates (@var{Codependencies}), a list of independent @var{Modules}
%       and a graph of dependency (@var{Graph}),
%       return the stratification of all modules given in @var{Strata}. Main call for the stratification module.
%   @param Codependencies A list of lists of codependencies in modules. It has the form [[CodependentPredicate1, CodependentPredicate2, ...][CodepsModule2], ...].
%       For example, in Test 3, it is [[c,b], [p,q]].
%   @param Modules A list of lists of predicates within an independent module. It has the form [[Predicate1, Predicate2, ...][Module2], ...].
%       For example, in Test 3, it is [[a,b,c,d,e,f,g,h], [o,p,q,r,s]].
%   @param Graph A list of lists of relationships of dependency in modules. It has the form [[[Predicate, [DependantPredicate1, DependantPredicate2, ...],[DependencyPredicate1, DependencyPredicate2, ...]], Dependency2, ...][DepsModule2], ...].
%       For example, in Test 3, it is [[[a, [], [b]], [b, [a, c], [c, d]], ...],[o, [r], [p]], [p, [o,q], [q]], [q, [p,r], [p, s]], [r, [], [o,q]], [s, [q], []]]].
%   @param Strata Return list of strata with the form [[[Stratum1-a],[Stratum1-b],...][[Stratum2-a],[Stratum2-b],...]...]. It is not ordered by level of stratum.
%       For example, in Test 3, it is [[0, [[[r], [a]]]], [2,[[[d], [g], [s]]]], [1,[[p,q], [c,b], [o], [e]]], [3, [[[h], [f]]]]].
create_strata(Codependencies, Modules, Graph, Strata):-
    generate_strata(Codependencies, Modules, Graph, UnconsolidatedStrata),
    join_codependencies(UnconsolidatedStrata, Codependencies, Strata).

create_unified_strata(Codependencies, Modules, Graph, Strata):-
    generate_strata(Codependencies, Modules, Graph, UnconsolidatedStrata),
    unify_strata(UnconsolidatedStrata, Codependencies, Strata). 


%% ------------------------------------------------------------- %%
%%                    Modify existing strata                     %%

%!  unify_strata(+Strata:list, +Codependencies:list, -Unified:list)
%   Given a list of independent strata and a list of relationships of codependency between predicates,
%   unify all strata in a sole main strata with its predicates grouped by dependency in relationships within a same stratum.
unify_strata(Strata, Codependencies, Unified):-
    unite_levels(Strata, [], NewStrata),
    join_codependencies(NewStrata, Codependencies, Unified).

%% ------------------------------------------------------------- %%
%%                       Generate strata                         %%

%!  generate_strata(+Codependencies:list, +Modules:list, +Graph:list, -UnconsolidatedStrata:list)
%   Given a list of codependent predicates (@var{Codependencies}), a list of independent @var{Modules}
%   and a graph of dependency (@var{Graph}),
%   return a list of strata (each module is stratificated in separated strata).
generate_strata(_,[],_,[]).
generate_strata(Codependencies, Modules, Graph, UnconsolidatedStrata):-
    generate_strata_(Codependencies, Modules, [], Graph, UnconsolidatedStrata).

generate_strata_(_Codependencies, [], Acc, _Graph, UnconsolidatedStrata):-
    reverse(Acc, UnconsolidatedStrata).

generate_strata_(Codependencies, [M|Modules], Acc, Graph, UnconsolidatedStrata):-
    generate_module_strata(Codependencies, M, Graph, UnconsolidatedStrata1),
    generate_strata_(Codependencies, Modules, [UnconsolidatedStrata1|Acc], Graph, UnconsolidatedStrata).

% Codependencies = [[Chain1Module1][Chain2Module1]...]
% Module = [Module1] = List of predicates
% Graph = [[a, [DirectDependents], [DirectDependencies]], [b, [DirectDependents], [DirectDependencies]],...]
% Strata = [[[Module1Stratum1-a][Module1Stratum1-b]][[Module1Stratum2-a][Module1Stratum2-b]]...]
generate_module_strata(Codependencies, Module, Graph, Strata):-
    generate_name_relations(Module, Names, Relations),
    generate_clp_codependency_ecuations(Codependencies, Relations, Names, 0),
    generate_clp_dependency_ecuations(Module, Relations, Names, Graph, Codependencies, 0, Domain2),
    solve(Names, Domain2),
    parse_levels(Module, Relations, [], Strata).

solve(Variables, MaxDomain):-
    domain(Variables, 0, MaxDomain),
    labeling([], Variables).

parse_levels([], _, Strata, Strata).

parse_levels([P|Predicates], Relations, OldStrata, Strata):- % Level already formed
    member([P, Variable], Relations),
    member([Variable, OtherPredicates], OldStrata),
    select([Variable, OtherPredicates], OldStrata, IStrata),
    merge_lists(OtherPredicates, [P], NewPredicates),
    append(IStrata, [[Variable, NewPredicates]], IStrata2),
    parse_levels(Predicates, Relations, IStrata2, Strata).

parse_levels([P|Predicates], Relations, OldStrata, Strata):- % New level
    member([P, Variable], Relations),
    append(OldStrata, [[Variable, [P]]], IStrata),
    parse_levels(Predicates, Relations, IStrata, Strata).

% UTILS



%% ------------------------------------------------------------- %%
%% ------------------------------------------------------------- %%
%%                              Aux                              %%
%% ------------------------------------------------------------- %%
%% ------------------------------------------------------------- %%

% Create a list of variables to compute levels and their relation to the predicate they represent
generate_name_relations(Predicates, LevelNames,VariablePredicateRelations):-
    length(Predicates, L),
    length(LevelNames, L),
    generate_name_relations_(Predicates, LevelNames, VariablePredicateRelations).

generate_name_relations_([], [], []).

generate_name_relations_([P|Predicates], [L|Levels], VariablePredicateRelations):-
    generate_name_relations_(Predicates, Levels, VariablePredicateRelations2),
    append([[P,L]], VariablePredicateRelations2, VariablePredicateRelations).


% CODEPS
% generate ecuations in variables present in Names
generate_clp_codependency_ecuations([], _R, _N, _). % No more codeps

generate_clp_codependency_ecuations([C|Codependencies], Relations, Names, Domain):-
    C = [A|Codeps],
    member([A, _], Relations), % Relevant codependency
    generate_clp_ecuations(A, Codeps, Relations, Names, '=', Domain, Domain),
    generate_clp_codependency_ecuations(Codependencies, Relations, Names, Domain).

generate_clp_codependency_ecuations([_|Codependencies], Relations, Names, Domain):- % That group of codeps wasn't relevant
    generate_clp_codependency_ecuations(Codependencies, Relations, Names, Domain).

% DEPS
generate_clp_dependency_ecuations([], _Relations, _Ecuations, _, _, Domain, Domain).
generate_clp_dependency_ecuations([P|Predicates], Relations, Ecuations, Graph, Codependencies, InitialDomain, FinalDomain):-
    find_all_direct_dependencies(P, Deps, Graph, 1), % find all direct dependencies
    flatten_list(Deps, FlattenDeps),
    filter_codeps(P, FlattenDeps, Codependencies, FilteredDeps), % find direct dependencies that are not codependencies
    generate_clp_dependency_ecuations_(P, Relations, Ecuations, FilteredDeps, InitialDomain, Domain1),
    generate_clp_dependency_ecuations(Predicates, Relations, Ecuations, Graph, Codependencies, Domain1, FinalDomain).

generate_clp_dependency_ecuations_(_, _R, _E, [], Domain, Domain).

generate_clp_dependency_ecuations_(Predicate, Relations, Ecuations, [D|FilteredDeps], InitialDomain, FinalDomain):-
    generate_clp_ecuations(Predicate, [D], Relations, Ecuations, '<', InitialDomain, Domain1),
    generate_clp_dependency_ecuations_(Predicate, Relations, Ecuations, FilteredDeps, Domain1, FinalDomain).

generate_clp_dependency_ecuations_(Predicate, Relations, Ecuations, [D|FilteredDeps], InitialDomain, FinalDomain):-
    generate_clp_ecuations(Predicate, [D], Relations, Ecuations, '=', InitialDomain, Domain1),
    generate_clp_dependency_ecuations_(Predicate, Relations, Ecuations, FilteredDeps, Domain1, FinalDomain).

% COMMON
generate_clp_ecuations(_, [], _R, _E, _, Domain, Domain):-!.

generate_clp_ecuations(Predicate, [T|Targets], Relations, Ecuations, Type, InitialDomain, FinalDomain):-
    member([T, TargetLevel], Relations),
    member([Predicate, Level], Relations), % get level variable name
    generate_clp_ecuation(Level, TargetLevel, Type, InitialDomain, Domain1), % Generate variable value = ecuation of level
    generate_clp_ecuations(Predicate, Targets, Relations, Ecuations, Type, Domain1, FinalDomain),!. % repeat for other targets

generate_clp_ecuation(VariableA, VariableB, '=', Domain, Domain):-
    VariableA #= VariableB,
    !.

generate_clp_ecuation(VariableA, VariableB,'<', OldDomain, NewDomain):-
    VariableA #< VariableB,
    NewDomain is OldDomain + 1,
    !.

% AUX

filter_codeps(Predicate, Dependencies, Codependencies, Filtered):-
    get_codependencies_group(Predicate, Codependencies, Codeps),
    select_non_repeated_elements_beta(Dependencies, Codeps, Filtered).

filter_codeps(_, Dependencies, _, Dependencies):- !.

%%% UNIFY LEVELS

unite_levels([], United, United).

unite_levels([S|Strata], Acc, United):-
    S = [Level, Ps],
    member([Level, Ps2], Acc),
    merge_lists(Ps2, Ps, Ps3),
    select([Level, Ps2], Acc, Acc2),
    append(Acc2, [[Level, Ps3]], Acc3),
    unite_levels(Strata, Acc3, United).

unite_levels([S|Strata], Acc, United):-
    append(Acc, [S], Acc1),
    unite_levels(Strata, Acc1, United).
