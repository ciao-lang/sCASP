:- module(stratification, [
    stratification/4,
    stratification_short/4,
    new_sub_strata/4,
    extract_sub_strata/4,
    lower_predicates/3,
    lower_facts/4,
    upper_roots/4,
    get_lower_level/2,
    upper_predicates/3,
    get_upper_level/2
]).

%% ------------------------------------------------------------- %%
:- use_package(assertions).
:- doc(filetype, module).

:- doc(module, "

This module creates different strata based in the relation of dependency between predicates of the same module.
Given two or more modules, it will join the individual results in an unique strata, grouping predicates with the same level in the same stratum.
").

%% ------------------------------------------------------------- %%

:- use_module('./clp_stratification').
:- use_module('../../common_dependencies/graph/graph', [sub_branch/4, find_all_dependencies/4, find_leaves/4]).
:- use_module('../modularization/independent_modules', [find_simple_modules/4]).
:- use_module('../../common_dependencies/auxiliary_functions/auxiliary', [
    merge_lists/3,present_in_both/3,
    delete_all_occurrences/3, check_instantiated/1, get_predicate_heads/2,
    last_element/2, repeated_elements/3, select_non_repeated_elements_beta/3,
    destructive_append/3, append_non_repeated_list/3, flatten_list/2]).
:- use_module('./stratification_aux').

%% ------------------------------------------------------------- %%

%% ------------------------------------------------------------- %%
%%                          Main calls                           %%

%!  stratification(+Program:list, -Codependencies:list, -Modules:list, -Strata:list)
%       Given a program with dependents(@var{Program}),
%       return in @var{Strata} the stratification of all independent modules found.
%       Also returns the found independent @var{Modules} and @var{Codependencies} of the program.
%       Main call for the stratification module.
%   @param Program A list of lists of relationships of dependency in modules.
%       Main call for the stratification module.
%   @param Program A list of lists of relationships of dependency in modules.
%       It has the form [[Predicate, [DependantPredicate1, DependantPredicate2, ...],[[Predicate1InClause1, Predicate2InClause1], [Predicate1InClause2, ...]], ...]].
%       For example, in Test 3, it is [[a, [], [[b]]], [b, [a, c], [[c, d]]], ...],[o, [r], [[p]]], [p, [o,q], [[q]]], [q, [p,r], [[p, s]]], [r, [], [[o,q]]], [s, [q], [[]]]].
%   @param Strata Return list of strata with the form [[[Stratum1-a],[Stratum1-b],...][[Stratum2-a],[Stratum2-b],...]...]. It is not ordered by level of stratum.
%       For example, in Test 3, it is [[0, [[[r], [a]]]], [2,[[[d], [g], [s]]]], [1,[[p,q], [c,b], [o], [e]]], [3, [[[h], [f]]]]].

stratification(Program, Codependencies, Modules, Strata):-
    check_instantiated([Program]),
    get_predicate_heads(Program, Heads),
    find_codependencies(Heads, Program, Codependencies),
    find_simple_modules(Program, Heads, 1, Modules),
    create_strata(Codependencies, Modules, Program, Strata).

%!  stratification_short(+Program:list, +Codependencies:list, +Modules:list, -Strata:list)
% Create new strata given the codependencies found in a program, its
% modularization, and the original program in a graph that contains the
% dependents of each node
stratification_short(Program, Codependencies, Modules, Strata):-
    check_instantiated([Codependencies, Modules, Program]),
    create_strata(Codependencies, Modules, Program, Strata).

% Obtain the strata from a given root
new_sub_strata(Root, Program, [SubModule], SubStrata):-
    check_instantiated([Root, Program]),
    get_predicate_heads(Program, Heads),
    member(Root, Heads),
    find_codependencies(Heads, Program, Codependencies),
    % Create submodule
    find_all_dependencies(Root, Dependencies, Program, 1),
    flatten_list(Dependencies, DependenciesFlatten),
    merge_lists(DependenciesFlatten, [Root], SubModule),
    sub_branch(SubModule, Program, 1, SubProgram),
    present_in_both(Codependencies, SubModule, Subcodependencies),
    delete_all_occurrences([], Subcodependencies, SubcodependenciesCleaned),
    create_strata(SubcodependenciesCleaned, [SubModule], SubProgram, SubStrata).

new_sub_strata(Root, Program, Modules, SubStrata):-
    check_instantiated([Root, Program]),
    get_predicate_heads(Program, Heads),
    \+member(Root, Heads),
    stratification(Program, _Codependencies, Modules, SubStrata).

% Obtain the strata from a given root and a given original strata
extract_sub_strata(Root, Program, Strata, SubStrata):-
    check_instantiated([Root, Program, Strata]),
    find_all_dependencies(Root, Dependencies, Program, 1),
    merge_lists(Dependencies, [Root], SubModule),
    extract_strata(SubModule, Program, SubStrata).

% If given strata is not valid, return new strata
extract_sub_strata(Root, Program, _Strata, SubStrata):-
    check_instantiated([Root, Program]),
    new_sub_strata(Root, Program, _, SubStrata).

%% ------------------------------------------------------------- %%
%%                    Modify existing strata                     %%

lower_facts(Strata, Modules, Graph, NewStrata):-
    lower_facts_(Strata, Modules, [], Graph, NewStrata).

lower_facts_([], [], Acc, _Graph, NewStrata):-
    reverse(Acc, NewStrata).

lower_facts_([S|Strata], [M|Modules], Acc, Graph, NewStrata):-
    lower_facts_module(S, M, Graph, NS),
    lower_facts_(Strata, Modules, [NS|Acc], Graph, NewStrata).

lower_facts_module(Strata, Module, Graph, NewStrata):-
    find_leaves(Module, Facts, Graph, 1),
    lower_predicates(Strata, Facts, NewStrata).

upper_roots(Strata, Modules, Graph, NewStrata):-
    find_roots_modules(Modules, Graph, Roots),
    upper_predicates(Strata, Roots, NewStrata).

lower_predicates(OldStrata, Predicates, NewStrata):-
    get_lower_level(OldStrata, LowerLevel),
    replace_level(Predicates, OldStrata, LowerLevel, NewStrata).

get_lower_level(Strata, LowerLevel):-
    sort(Strata, Sorted),
    last_element(Sorted, Last),
    Last = [LowerLevel|_].

upper_predicates(OldStrata, Predicates, NewStrata):-
    get_upper_level(OldStrata,SuperiorLevel),
    replace_level(Predicates, OldStrata, SuperiorLevel, NewStrata).

get_upper_level(Strata, SuperiorLevel):-
    sort(Strata, Sorted),
    Sorted = [[SuperiorLevel|_]|_].
