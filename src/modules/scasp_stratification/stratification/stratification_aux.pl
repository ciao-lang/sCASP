:- module(stratification_aux, _).
:- use_module(library(lists)).
:- use_module('../../common_dependencies/auxiliary_functions/auxiliary.pl', [
    merge_lists/3, repeated_elements/3, present_in_both/3,
    delete_all_occurrences/3, select_non_repeated_elements_beta/3,
    flatten_list/2, append_non_repeated_list/3]).
:- use_module('../../common_dependencies/graph/graph', [find_leaves/4,find_roots/4, indirect_dependency/5]).

% AUX
find_leaves_modules([], _, []).

find_leaves_modules([M|Modules], Graph, Leaves):-
    find_leaves(M, Leaves1, Graph, 1),
    find_leaves_modules(Modules, Graph, Leaves2),
    merge_lists(Leaves2, Leaves1, Leaves).

find_roots_modules([], _, []).

find_roots_modules([M|Modules], Graph, Roots):-
    find_roots(M, Roots1, Graph, 1),
    find_roots_modules(Modules, Graph, Roots2),
    merge_lists(Roots2, Roots1, Roots).

get_codependencies_group(S, [C|_], C):-
    member(S, C).

get_codependencies_group(S, [_|Codependencies], Group):-
   get_codependencies_group(S, Codependencies, Group), !.

join_codependencies(Modules, Codependencies, Joined):-
    join_codependencies_(Modules, [], Codependencies, Joined).

join_codependencies_([], Acc, _Codependencies, Joined):-
    reverse(Acc, Joined).

join_codependencies_([M|Modules], Acc, Codependencies, Joined):-
    join_codependencies_module(M, Codependencies, J),
    join_codependencies_(Modules, [J|Acc], Codependencies, Joined).

join_codependencies_module([],_,[]).
join_codependencies_module([[L,S]|Strata], Codependencies, [[L,U]|Unified]):-
    separate_codependencies(S, Codependencies, U1),
    clean(U1, U),
    join_codependencies_module(Strata, Codependencies, Unified).

separate_codependencies([], _, [[],[]]).
separate_codependencies([S|St], Codependencies, [C,N]):-
    get_codependencies_group(S, Codependencies, C1),
    select_non_repeated_elements_beta(St, C1, NewSt),
    separate_codependencies(NewSt, Codependencies, [C2, N]),
    append([C1], C2, C).

separate_codependencies([S|St], Codependencies, [C,N]):-
    separate_codependencies(St, Codependencies, [C, N1]),
    append([[S]], N1, N).

clean(X, Y):-
    clean1(X, X1),
    clean2(X1, Y).
%    clean3(X2, Y).

% [[], [_]] -> [[_]]
clean1([], []):-!.

clean1([[], N], N):-!.

clean1([N, []], N):-!.

clean1([N, M], [N, M]):-!.

% [[[Codeps1], [Codeps2]], [indep1]] -> [[C1], [C2], [I]]
clean2([N, M], O):-
    N = [[_|_]|_],
    merge_lists(N,M,O).

clean2([N,M],[N,M]):-!.

clean2(N,N):-!.

clean3([M], M):-
    M = [N|_],
    length(N,_).

clean3([M|Ms], N):-
    \+atom(M),
    clean3(Ms, Ms1),
    merge_lists(M, Ms1, N).

clean3(N,N).

replace_level([], Strata, _, Strata).

replace_level([P|Predicates], OldStrata, NewLevel, NewStrata):-
    member([NewLevel, LevelPredicates], OldStrata),
    appear_in_level(P, LevelPredicates, _),
    replace_level(Predicates, OldStrata, NewLevel, NewStrata).

replace_level([P|Predicates], OldStrata, NewLevel, NewStrata):-
    member([OldLevel, LevelPredicates], OldStrata),
    appear_in_level(P, LevelPredicates, OtherPredicates),
    select([OldLevel, _], OldStrata, IntermediateStrata),
    member([NewLevel, TargetLevelPredicates], OldStrata),
    select([NewLevel, _], IntermediateStrata, IntermediateStrata2),
    merge_lists(TargetLevelPredicates, [[P]], NewTargetLevelPredicates),
    append([[OldLevel, OtherPredicates]], IntermediateStrata2, IntermediateStrata3),
    append([[NewLevel, NewTargetLevelPredicates]], IntermediateStrata3, IntermediateStrata4),
    replace_level(Predicates, IntermediateStrata4, NewLevel, NewStrata).

extract_strata(_, [], []).

extract_strata([], _, []).

extract_strata(Predicates, [S|Strata], SubStrata):-
    S = [Level, StrataPredicates],
    present_in_both(StrataPredicates, Predicates, SP1),
    delete_all_occurrences([], SP1, SP2),
    S1 = [Level, SP2],
    extract_strata(Predicates, Strata, S2),
    append([S1], S2, SubStrata).

appear_in_level(Predicate, Level, OtherPredicates):-
    member([Predicate], Level),
    select([Predicate], Level, OtherPredicates).

appear_in_level(Predicate, Level, OtherPredicates):-
    member(Ps, Level),
    member(Predicate, Ps),
    select(Predicate, Ps, OtherPredicates1),
    select(Ps, Level, OtherPredicates2),
    append(OtherPredicates1, [OtherPredicates2], OtherPredicates).

%% ------------------------------------------------------------- %%
%%                      Codependencies                           %%

find_codependencies(Heads, Program, Codependencies):-
    find_codependencies_(Heads, Program, [], Codependencies).

find_codependencies_([], _, Acc, Codependencies):-
    clean_codependencies(Acc, Codependencies).

find_codependencies_([H|Heads], Program, Acc, Codependencies):-
    find_codependencies_predicate(H, Heads, Program, Acc, NewAcc),
    find_codependencies_(Heads, Program, NewAcc, Codependencies).

find_codependencies_predicate(_Predicate, [], _Program, Codependencies, Codependencies).

find_codependencies_predicate(Predicate, [Predicate|Heads], Program, Acc, Codependencies):-
    find_codependencies_predicate(Predicate, Heads, Program, Acc, Codependencies).

find_codependencies_predicate(Predicate, [H|Heads], Program, Acc, Codependencies):-
    indirect_dependency(Predicate, H, Program, 1, Result),
    indirect_dependency(H, Predicate, Program, 1, Result2),
    (
        Result = 1,
        Result2 = 1,
        find_codependencies_predicate(Predicate, Heads, Program, [[Predicate, H]|Acc], Codependencies)
    );(
        find_codependencies_predicate(Predicate, Heads, Program, Acc, Codependencies)
    ).

clean_codependencies(Current, Codependencies):-
    clean_codependencies_(Current, [], Codependencies).

clean_codependencies_([], Codependencies, Codependencies).

clean_codependencies_([C|Current], Seen, Codependencies):-
    append_non_repeated_list(C, Seen, NewSeen),
    clean_codependencies_(Current, NewSeen, Codependencies).