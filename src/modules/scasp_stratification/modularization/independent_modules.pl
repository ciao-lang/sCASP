:- module(independent_modules, [
    find_modules/6,
    find_simple_modules/4
    ]).


%% ------------------------------------------------------------- %%

:- use_package(assertions).
:- doc(filetype, module).

:- doc(module, "

This module calls submodules TODO.

").

%% ------------------------------------------------------------- %%
:- use_module('../../common_dependencies/auxiliary_functions/auxiliary.pl', [
    merge_lists/3, head/2, heads/2, statement_appear_in_module/2,
    appear/2, check_instantiated/1, repeated_elements/3,
    select_non_repeated_elements_beta/3, destructive_append/3,
    flatten_list/2, append_non_repeated_list/3]).
:- use_module('../../common_dependencies/graph/graph', [list_connected_nodes/4]).


%% ------------------------------------------------------------- %%
%%                            Main                               %%

% Find a list of connected predicates (simple modules) and form a subprogram
% with the clauses of the predicates defined in each module.

find_modules(Rules, StatementList, Graph, GraphWithDependents, SimpleModules, Modules):-
    check_instantiated([Graph, StatementList, Graph]),
    list_connected_nodes(StatementList, Graph, GraphWithDependents, SimpleModules),
    append_statements(SimpleModules, Rules, Modules).

% Find a list of connected predicates (simple modules)
find_simple_modules(Graph, StatementList, GraphWithDependents, SimpleModules):-
    check_instantiated([Graph, StatementList]),
    list_connected_nodes(StatementList, Graph, GraphWithDependents, SimpleModules).


%% ------------------------------------------------------------- %%
%%                             Aux                               %%

% Retrieve all statements of predicates present in a dependency list
append_statements([], _, []).
append_statements([D|DependencyLists], Program, Modules):-
    append_statements_module(D, Program, Module1),
    append_statements(DependencyLists, Program, Module2),
    append([Module1], Module2, Modules).

append_statements_module(_,[],[]).

append_statements_module(DependencyList, [S|Statements], Module):-
    statement_appear_in_module(S, DependencyList),
    append_statements_module(DependencyList, Statements, Module2),
    append([S],Module2, Module).

append_statements_module(DependencyList, [_|Statements], Module):-
    append_statements_module(DependencyList, Statements, Module).
