:- module(main_stratification, [
    main_stratification/0
]).
%% ------------------------------------------------------------- %%
:- use_package(assertions).
%% ------------------------------------------------------------- %%
:- use_module(library(lists)).
:- use_module('./stratification/stratification').
:- use_module('../common_dependencies/auxiliary_functions/auxiliary').
:- use_module('../common_dependencies/graph/graph').
:- reexport('../../sasp/output', [
    pr_rule/2
                            ]).
:- use_module('../../scasp_io', [stratification_parameters/1, init_counter/0]).
:- use_module('../common_dependencies/print', [print_dcg/1]).
:- use_module('../common_dependencies/parser', [program_to_graph/2]).
%% ------------------------------------------------------------- %%
:- doc(section, "Main predicates").
:- pred main_stratification # "Makes preparations for stratification, and executes stratification/2 from module 'stratification'.".

main_stratification:-
    stratification_parameters([Root, Options]),
    % transform to graph
    program_to_graph(Graph, 1),
    clean_true_false(Graph, GraphCleaned),
    % call stratification/4
    (
        (
            Root = [],
            stratification(GraphCleaned, _Codependencies, Modules, Strata1)
        );(
            new_sub_strata(Root, GraphCleaned, Modules, Strata1)
        )
    ),
    % Apply transformations
    transform_strata(Options, Strata1, Graph, Modules, Strata),
    % print result
    print_strata(Strata, Graph), nl.

transform_strata([], Strata, _, _, Strata).

transform_strata([O|Options], InitialStrata, Graph, Modules, Strata):-
    transform_strata_(O, InitialStrata, Graph, Modules, IntermediateStrata),
    transform_strata(Options, IntermediateStrata, Graph, Modules, Strata).

transform_strata_('l', InitialStrata, Graph, Modules, Strata):-
    lower_facts(InitialStrata, Modules, Graph, Strata).

transform_strata_('u', InitialStrata, Graph, Modules, Strata):-
    upper_roots(InitialStrata, Modules, Graph, Strata).

transform_strata_(_, Strata, _Graph, _Modules, Strata).

% main_splitting:-
%     % obtain predicate to stratify from
%     splitting_parameters(InitialPredicate),
%     % transform to graph
%     program_to_graph(Graph, 1),
%     % call splitting/7
%     % splitting(PredicatesToForget, Graph, ForgottenGraph, SCASPExec),
%     % print result
%     print_dcg(ForgottenGraph).

clean_true_false(Graph, GraphCleaned):-
    delete_in_lists_of_lists(true, Graph, Graph1),
    delete_in_lists_of_lists(false, Graph1, GraphCleaned).

print_strata(Strata, Graph):-
    print_strata_(Strata, Graph, 1).

print_strata_([], _, _).

print_strata_([S|Strata], Graph, Number):-
    sort(S, Sorted),
    display('% Module '),
    display(Number),
    display(': '),
    display(Sorted), nl, nl,
    Number2 is Number + 1,
    print_strata_program(Sorted, Graph),
    print_strata_(Strata, Graph, Number2).

print_strata_program([], _Graph).

print_strata_program([Stratum|Strata], Graph):-
    print_stratum_program(Stratum, Graph),
    print_strata_program(Strata, Graph).

print_stratum_program([Level, Stratum], Graph):-
    flatten_list(Stratum, Predicates),
    get_rules(Predicates, Graph, Rules),
    restore_facts_missing_predicates(Rules, RulesCleaned),
    print_level(Level, RulesCleaned).

print_level(_Level, []) :- !.

print_level(Level, Rules):-
    display('%% Level '),
    display(Level), nl, nl,
    print_dcg(Rules), nl.

get_rules(Predicates, Graph, Rules):-
    get_rules_(Predicates, [], Graph, Rules).

get_rules_([], Acc, _Graph, Rules):-
    reverse(Acc, Rules).

get_rules_([P|Predicates], Acc, Graph, Rules):-
    member([P, _, Dependencies], Graph),
    get_rules_(Predicates, [[P, Dependencies]|Acc], Graph, Rules).