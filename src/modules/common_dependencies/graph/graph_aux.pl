:- module(graph_aux, [
    find_all_exact_direct_dependents_/5,
    list_connected_nodes_/5,
    indirect_dependency_/6,
    indirect_dependency_multiple/6,
    find_all_direct_dependencies_/4,
    find_all_exact_direct_dependencies_/4,
    find_all_dependencies_/5,
    find_all_exact_dependencies_/5,
    find_all_direct_dependents_/4,
    find_all_exact_direct_dependents_/5,
    find_all_dependents_/5,
    find_all_exact_dependents_/5,
    add_value_to_leaf_/5,
    rules_to_graph_/4,
    add_dependents_/3,
    find_path_/7
]).

%% ------------------------------------------------------------- %%
:- use_package(assertions).
:- doc(filetype, module).

:- doc(module, "

Auxiliary functions for graph creation and management.

").

%% ------------------------------------------------------------- %%

:- use_module('../auxiliary_functions/auxiliary').

%% ------------------------------------------------------------- %%
%%                         Dependencies                          %%
%% ------------------------------------------------------------- %%


list_connected_nodes_([], _, _, Modules, Modules).

list_connected_nodes_([S|StatementList], Graph, GraphWithDependents, Acc, SimpleModules):-
    find_all_direct_dependencies_(S, Dependencies1, Graph, GraphWithDependents),
    flatten_list(Dependencies1, Dependencies),
    find_all_exact_direct_dependents_(S, [], Dependents, Graph, GraphWithDependents),
    merge_lists(Dependencies, Dependents, ConnectedNodes),
    merge_lists(ConnectedNodes,[S], Module),
    append_non_repeated_list(Module, Acc, Acc2),
    list_connected_nodes_(StatementList, Graph, GraphWithDependents, Acc2, SimpleModules).

indirect_dependency_(Dependent, Dependency, Graph, _Visited, GraphWithDependents, 1):-
    find_all_dependencies_(Dependent, Dependencies, [], Graph, GraphWithDependents),
    flatten_list(Dependencies, DD),
    multiple_goal(Dependency, DD), !.

indirect_dependency_(Dependent, _Dependency, _Graph, Visited, _GraphWithDependents,  0):-
    member(Dependent, Visited), !.

indirect_dependency_(Dependent, Dependency, Graph, Visited, GraphWithDependents,  Result):-
    \+member(Dependent, Visited),
    find_all_dependencies_(Dependent, Dependencies, [], Graph, GraphWithDependents),
    flatten_list(Dependencies, DD),
    indirect_dependency_multiple(DD, Dependency, Graph, [Dependent|Visited], GraphWithDependents, Result), !.

% explore dependencies
indirect_dependency_multiple([], _Dependency, _Graph, _Visited, _GraphWithDependents, 0) :- !.

indirect_dependency_multiple([D|Dependents], Dependency, Graph, Visited, GraphWithDependents, Result):-
    indirect_dependency_(D, Dependency, Graph, Visited, GraphWithDependents, ResultD),
    (
        ResultD = 1,
        Result = 1
    );(
        indirect_dependency_multiple(Dependents, Dependency, Graph, [D|Visited], GraphWithDependents, Result)
    ).

% find direct dependencies
find_all_direct_dependencies_(not(not(Predicates)), Dependencies, Graph, GraphWithDependents):-
    find_all_exact_direct_dependencies_(Predicates, Dependencies1, Graph, GraphWithDependents),
    find_all_exact_direct_dependencies_(not(Predicates), Dependencies2, Graph, GraphWithDependents),
    find_all_exact_direct_dependencies_(not(not(Predicates)), Dependencies3, Graph, GraphWithDependents),
    merge_lists(Dependencies1, Dependencies2, Dependencies4),
    merge_lists(Dependencies3, Dependencies4, Dependencies), !.

find_all_direct_dependencies_(not(Predicates), Dependencies, Graph, GraphWithDependents):-
    find_all_exact_direct_dependencies_(Predicates, Dependencies1, Graph, GraphWithDependents),
    find_all_exact_direct_dependencies_(not(Predicates), Dependencies2, Graph, GraphWithDependents),
    find_all_exact_direct_dependencies_(not(not(Predicates)), Dependencies3, Graph, GraphWithDependents),
    merge_lists(Dependencies1, Dependencies2, Dependencies4),
    merge_lists(Dependencies3, Dependencies4, Dependencies), !.

find_all_direct_dependencies_(Predicates, Dependencies, Graph, GraphWithDependents):-
    find_all_exact_direct_dependencies_(Predicates, Dependencies1, Graph, GraphWithDependents),
    find_all_exact_direct_dependencies_(not(Predicates), Dependencies2, Graph, GraphWithDependents),
    find_all_exact_direct_dependencies_(not(not(Predicates)), Dependencies3, Graph, GraphWithDependents),
    merge_lists(Dependencies1, Dependencies2, Dependencies4),
    merge_lists(Dependencies3, Dependencies4, Dependencies), !.

% find direct dependencies
find_all_exact_direct_dependencies_(Predicate, Dependencies, Graph, 0):-
    member([Predicate, Dependencies], Graph), !.

find_all_exact_direct_dependencies_(Predicate, Dependencies, Graph, 1):-
    member([Predicate, _, Dependencies], Graph), !.

find_all_exact_direct_dependencies_(Predicate, [], Graph, _):-
    \+member([Predicate|_], Graph), !.

% find direct and indirect dependencies
find_all_dependencies_([], [], [], _, _).
find_all_dependencies_(Predicate, Dependencies, [], Graph, GraphWithDependents):- % find sons
    \+length(Predicate, _),
    find_all_direct_dependencies_(Predicate, Sons, Graph, GraphWithDependents),
    flatten_list(Sons, SonsF),
    find_all_dependencies_(SonsF, DependenciesSons, [], Graph, GraphWithDependents),
    merge_lists(SonsF, DependenciesSons, Dependencies), !.

% find indirect dependencies
find_all_dependencies_([P|Sons], Grandsons,Saw, Graph, GraphWithDependents):-
    \+member(P, Saw),
    find_all_direct_dependencies_(P, GrandsonsP, Graph, GraphWithDependents),
    flatten_list(GrandsonsP, GrandsonsPF),
    merge_lists(Sons, GrandsonsPF, NewSons),
    find_all_dependencies_(NewSons, Grandsons2, [P|Saw], Graph, GraphWithDependents),
    merge_lists(GrandsonsPF, Grandsons2, Grandsons), !.

find_all_dependencies_([_|Sons], Grandsons, Saw, Graph, GraphWithDependents):- 
    find_all_dependencies_(Sons, Grandsons, Saw, Graph, GraphWithDependents), !.

find_all_dependencies_(_,[],_,_,_):-!.

find_all_exact_dependencies_([], [], _, _, _).
find_all_exact_dependencies_(Predicate, Dependencies, [], Graph, GraphWithDependents):- % find sons
    find_all_exact_direct_dependencies_(Predicate, Sons, Graph, GraphWithDependents),
    find_all_exact_dependencies_(Sons, DependenciesSons, [], Graph, GraphWithDependents),
    merge_lists(Sons, DependenciesSons, Dependencies), !.

% find indirect dependencies
find_all_exact_dependencies_([P|Sons], Grandsons,Saw, Graph, GraphWithDependents):-
    \+member(P, Saw),
    find_all_exact_direct_dependencies_(P, GrandsonsP, Graph, GraphWithDependents),
    merge_lists(Sons, GrandsonsP, NewSons),
    find_all_exact_dependencies_(NewSons, Grandsons2, [P|Saw], Graph, GraphWithDependents),
    merge_lists(GrandsonsP, Grandsons2, Grandsons), !.

find_all_exact_dependencies_([_|Sons], Grandsons, Saw, Graph, GraphWithDependents):- 
    find_all_exact_dependencies_(Sons, Grandsons, Saw, Graph, GraphWithDependents), !.

find_all_exact_dependencies_(_,[],_,_,_):-!.

%% ------------------------------------------------------------- %%
%%                          Dependents                           %%
%% ------------------------------------------------------------- %%

% find direct dependents

find_all_direct_dependents_(not(not(Predicate)), Dependents, Graph, GraphWithDependents):-
    find_all_exact_direct_dependents_(Predicate, [], Dependents1, Graph, GraphWithDependents),
    find_all_exact_direct_dependents_(not(Predicate), [], Dependents2, Graph, GraphWithDependents),
    find_all_exact_direct_dependents_(not(not(Predicate)), [], Dependents3, Graph, GraphWithDependents),
    merge_lists(Dependents1, Dependents2, Dependents4),
    merge_lists(Dependents3, Dependents4, Dependents), !.


find_all_direct_dependents_(not(Predicate), Dependents, Graph, GraphWithDependents):-
    find_all_exact_direct_dependents_(Predicate, [], Dependents1, Graph, GraphWithDependents),
    find_all_exact_direct_dependents_(not(Predicate), [], Dependents2, Graph, GraphWithDependents),
    find_all_exact_direct_dependents_(not(not(Predicate)), [], Dependents3, Graph, GraphWithDependents),
    merge_lists(Dependents1, Dependents2, Dependents4),
    merge_lists(Dependents3, Dependents4, Dependents), !.


find_all_direct_dependents_(Predicate, Dependents, Graph, GraphWithDependents):-
    find_all_exact_direct_dependents_(Predicate, [], Dependents1, Graph, GraphWithDependents),
    find_all_exact_direct_dependents_(not(Predicate), [], Dependents2, Graph, GraphWithDependents),
    find_all_exact_direct_dependents_(not(not(Predicate)), [], Dependents3, Graph, GraphWithDependents),
    merge_lists(Dependents1, Dependents2, Dependents4),
    merge_lists(Dependents3, Dependents4, Dependents), !.

find_all_exact_direct_dependents_(Predicate, _, Dependents, Graph, 1):-
    member([Predicate, Dependents, _], Graph), !.

find_all_exact_direct_dependents_(_Predicate, _, [], _Graph, 1).

find_all_exact_direct_dependents_(_Predicate, Dependents, Dependents, [], 0) :- !.

find_all_exact_direct_dependents_(Predicate, Acc, Dependents, [[Dependent, Dependencies]|Graph], 0):-
    appear(Predicate, Dependencies),
    find_all_exact_direct_dependents_(Predicate, [Dependent|Acc], Dependents, Graph, 0).

find_all_exact_direct_dependents_(Predicate, Acc, Dependents, [_|Graph], 0):-
    find_all_exact_direct_dependents_(Predicate, Acc, Dependents, Graph, 0).

% find direct and indirect dependents
find_all_dependents_(Predicate, Dependents, [], Graph, GraphWithDependents):-
    find_all_direct_dependents_(Predicate, Parents, Graph, GraphWithDependents),
    find_all_dependents_(Parents, Grandparents, [], Graph, GraphWithDependents),
    merge_lists(Parents, Grandparents, Dependents).

% find indirect dependents
find_all_dependents_([], [], _, _, _).

find_all_dependents_([P|Parents], Grandparents,Saw, Graph, GraphWithDependents):-
    \+member(P, Saw),
    find_all_direct_dependents_(P, GrandparentsP, Graph, GraphWithDependents),
    merge_lists(Parents, GrandparentsP, NewParents),
    find_all_dependents_(NewParents, Grandparents2, [P|Saw], Graph, GraphWithDependents),
    merge_lists(GrandparentsP, Grandparents2, Grandparents).

find_all_dependents_([_|Parents], Grandparents,Saw, Graph, GraphWithDependents):- 
    find_all_dependents_(Parents, Grandparents,Saw, Graph, GraphWithDependents).

% find exact indirect dependents
find_all_exact_dependents_([], [], _, _, _).

find_all_exact_dependents_([P|Parents], Grandparents,Saw, Graph, GraphWithDependents):-
    \+member(P, Saw),
    find_all_exact_direct_dependents_(P, [], GrandparentsP, Graph, GraphWithDependents),
    merge_lists(Parents, GrandparentsP, NewParents),
    find_all_exact_dependents_(NewParents, Grandparents2, [P|Saw], Graph, GraphWithDependents),
    merge_lists(GrandparentsP, Grandparents2, Grandparents).

find_all_exact_dependents_([_|Parents], Grandparents,Saw, Graph, GraphWithDependents):- 
    find_all_exact_dependents_(Parents, Grandparents,Saw, Graph, GraphWithDependents).

%% ------------------------------------------------------------- %%
%%                        Graph utilities                        %%
%% ------------------------------------------------------------- %%


add_value_to_leaf_(OldGraph, Leaf, Value, NewGraph, 0):-
    (select([Leaf|_], OldGraph, Graph);
    Graph = OldGraph),
    avoid_triple(Value, Value1),
    NewGraph = [[Leaf, Value1]|Graph].

add_value_to_leaf_(OldGraph, Leaf, Value, NewGraph, 1):-
    (select([Leaf, Depts, _], OldGraph, Graph);
    OldGraph = Graph),
    avoid_triple(Value, Value1),
    NewGraph = [[Leaf, Depts, Value1]|Graph].

rules_to_graph_([], _, [], []). % Empty input
rules_to_graph_([], _, Sorted, Sorted). % Exit

rules_to_graph_([R|Rules], Acc, Acc1, Sorted):- % Already sorted
    R =.. [.,Head1,_Body], % get head and body
    Head1 =.. [Head|X],
    length(X, Length),
    member([Head, Length], Acc),
    rules_to_graph_(Rules, Acc, Acc1, Sorted).

rules_to_graph_([P|RulesLeft], AlreadySorted, Acc, Sorted):-
    P =.. [.,Head1,Body], % get head and body
    Head1 =.. [Head|X],
    length(X, Length),
    find_rules(Head, Length, RulesLeft, [], Rules), 
    append(Body, Rules, AllRules),
    NewSet = [Head1, AllRules],
    append(Acc, [NewSet], NewSorted),
    append(AlreadySorted, [[Head, Length]], NewAlreadySorted),
    rules_to_graph_(RulesLeft, NewAlreadySorted, NewSorted, Sorted).

add_dependents_([], Graph, Checked):-
    check_dependents(Graph, [], Checked).

add_dependents_([Node|Graph], Acc, Final):-
    Node = [Predicate, Body],
    flatten_list(Body, Descendents),
    add_predicate_dependent(Predicate, Descendents, Acc, Acc1),
    add_dependents_(Graph, Acc1, Final).

%   Mark a certain predicate as the dependent of a given list
add_predicate_dependent(_, [], Marked, Marked).

% it already has dependents
add_predicate_dependent(Dependent, [Predicate|ListToMark], Marked, Final):-
    member([Predicate, Dependents, Descendents], Marked),
    merge_lists(Dependents, [Dependent], NewDependents),
    replace_keeping_order(Marked, [Predicate, Dependents, Descendents], [Predicate, NewDependents, Descendents], NewMarked),
    add_predicate_dependent(Dependent, ListToMark, NewMarked, Final).

% it does not have dependents
add_predicate_dependent(Dependent, [Predicate|ListToMark], Marked, Final):-
    member([Predicate, Descendents], Marked),
    replace_keeping_order(Marked, [Predicate, Descendents], [Predicate, [Dependent], Descendents], NewMarked),
    add_predicate_dependent(Dependent, ListToMark, NewMarked, Final).

% it is negated
add_predicate_dependent(Dependent, [not(Predicate)|ListToMark], Marked, Final):-
    add_predicate_dependent(Dependent, [Predicate|ListToMark], Marked, Final).

% it is missing
add_predicate_dependent(Dependent, [Predicate|ListToMark], Marked, Final):-
    merge_lists(Marked, [[Predicate, [Dependent], [[false]]]], NewMarked),
    add_predicate_dependent(Dependent, ListToMark, NewMarked, Final).

check_dependents([], Acc, Final):-
    check_instantiated([Acc]),
    reverse(Acc, Final).

check_dependents([], [], []).

check_dependents([[Predicate, Dependents, Descendents]|Potential], Acc, Final):-
    check_instantiated([Predicate, Dependents, Descendents, Potential, Acc]),
    check_dependents(Potential, [[Predicate, Dependents, Descendents]|Acc], Final).

check_dependents([[Predicate, Descendents]|Potential], Acc, Final):-
    check_instantiated([Predicate, Descendents, Potential, Acc]),
    check_dependents(Potential, [[Predicate, [], Descendents]|Acc], Final).


%% ------------------------------------------------------------- %%
%%                           Path                                %%
%% ------------------------------------------------------------- %%


find_path_(Start, End, Visited, PotentialPath, Graph, GraphWithDependents, Paths):-
    goal(Start, End),
    Visited = [],
    find_all_direct_dependencies_(Start, Dependencies, Graph, GraphWithDependents),
    flatten_list(Dependencies, FlatDependencies),
    explore(FlatDependencies, End, [Start|Visited], [Start|PotentialPath], Graph, GraphWithDependents, Paths2),
    (( 
        PotentialPath = [],
        Paths = Paths2
    );
        Paths = [[Start|PotentialPath]|Paths2]
    ), !.

find_path_(Start, End, Visited, PotentialPath, Graph, GraphWithDependents, Paths):-
    goal(Start, End),
    Visited = [],
    find_all_direct_dependencies_(Start, Dependencies, Graph, GraphWithDependents),
    flatten_list(Dependencies, FlatDependencies),
    explore(FlatDependencies, End, [Start|Visited], [Start|PotentialPath], Graph, GraphWithDependents, Paths2),
    ((
        PotentialPath = [],
        Paths = Paths2
    );
        Paths = [[Start|PotentialPath]|Paths2]
    ), !.

find_path_(Start, End, _Visited, PotentialPath, _Graph, _, Paths):-
    goal(Start, End),
    Paths = [[Start|PotentialPath]], !.

% Start has no dependencies
find_path_(Start, _End, _Visited, _, Graph, GraphWithDependents, []):-
    find_all_direct_dependencies_(Start, D, Graph, GraphWithDependents),
    (
        D = [];
        D = [[]]
    ), !.

find_path_(Start, _End, Visited, _, _Graph, _, []):-
    member(Start, Visited), !.

find_path_(Start, End, Visited, PotentialPath, Graph, GraphWithDependents, Paths):-
    find_all_direct_dependencies_(Start, Dependencies, Graph, GraphWithDependents),
    flatten_list(Dependencies, FlatDependencies),
    explore(FlatDependencies, End, [Start|Visited], [Start|PotentialPath], Graph, GraphWithDependents, Paths).

explore([], _, _, _, _, _, []).

explore([S|Starts], End, Visited, PotentialPaths, Graph, GraphWithDependents, Paths):-
    find_path_(S, End, Visited, PotentialPaths, Graph, GraphWithDependents, Paths1),
    explore(Starts, End, [S|Visited], PotentialPaths, Graph, GraphWithDependents,Paths2),
    append_paths(Paths1, Paths2, Paths).

goal(Start, End):-
    Start = End, !.

goal(Start, End):-
    Start = not(End), !.

goal(Start, End):-
    Start = not(not(End)), !.

goal(End, Start):-
    Start = not(End), !.

goal(End, Start):-
    Start = not(not(End)), !.

multiple_goal(Start, [G|_Goals]):-
    goal(Start, G).

multiple_goal(Start, [_G|Goals]):-
    multiple_goal(Start, Goals).

append_paths(Paths1, [], Paths1).

append_paths([], Paths, Paths).

append_paths([L|Paths1], Paths2, Paths):-
    append_path(L, Paths2, PathsL2),
    append_paths(Paths1, PathsL2, Paths).

append_path([], PathList, PathList).

append_path(NewPath, [], [NewPath]).

append_path(NewPath, PathList, NewPathList):-
    append_path_(NewPath, PathList, [], NewPathList).

append_path_(NewPath, [], PathListAcc, [NewPath|PathListAcc]).

% Reapeated path
append_path_(NewPath, [L|PathList], PathListAcc, NewPathList):-
    select_non_repeated_elements(NewPath, L, []),
    select_non_repeated_elements(L, NewPath, []),
    length(NewPath, N),
    length(L, N),
    destructive_append([L|PathList], PathListAcc, NewPathList).

append_path_(NewPath, [L|PathList], PathListAcc, NewPathList):-
    append_path_(NewPath, PathList, [L|PathListAcc], NewPathList).