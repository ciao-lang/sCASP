:- module(graph, [
    find_roots/4,
    is_root/3, 
    find_leaves/4,
    is_leaf/3,
    list_connected_nodes/4,
    indirect_dependency/5,
    find_all_direct_dependencies/4,
    find_all_exact_direct_dependencies/4,
    find_all_dependencies/4,
    find_all_exact_dependencies/4,
    find_all_direct_dependents/4,
    find_all_exact_direct_dependents/4,
    find_all_dependents/4,
    find_all_exact_dependents/4,
    rules_to_graph/3,
    add_dependents/2,
    delete_dependents/2,
    add_value_to_leaf/5,
    sub_branch/4,
    find_path/5
]).

%% ------------------------------------------------------------- %%
:- use_package(assertions).
:- doc(filetype, module).

:- doc(module, "

This module analyzes dependency between predicates to create a graph structure.
A program module is a list of statements with a relation of dependency between them.

If A -> B, then A is the dependent for B, and B is the dependency for A.

A node is a predicate present in the dependency graph.

A root is a node without dependents.
A leaf is a node without dependencies.

Relation between connected nodes (a sub-graph) can be a branch (have roots, leaves or both)
or a circle (have no roots nor leaves).
").

%% ------------------------------------------------------------- %%
:- use_module(library(lists)).
:- use_module('./graph_aux').
:- use_module('../auxiliary_functions/auxiliary', [
    append_non_repeated_list/3, check_instantiated/1, merge_lists/3, repeated_elements/3, replace_keeping_order/4
]).

%% ------------------------------------------------------------- %%
%%                    Roots and leaves                           %%
%% ------------------------------------------------------------- %%

find_roots([], [], _, _).

find_roots([P|PredicateList], Roots, Graph, GraphWithDependents):-
    check_instantiated([P, PredicateList, Graph, GraphWithDependents]),
    is_root(P, Graph, GraphWithDependents),
    find_roots(PredicateList, Roots2, Graph, GraphWithDependents),
    merge_lists(Roots2, [P], Roots).

find_roots([_|PredicateList], Roots, Graph, GraphWithDependents):-
    check_instantiated([PredicateList, Graph, GraphWithDependents]),
    find_roots(PredicateList, Roots, Graph, GraphWithDependents).

is_root(P, Graph, GraphWithDependents):-
    check_instantiated([P, Graph, GraphWithDependents]),
    find_all_direct_dependents(P, Deps, Graph, GraphWithDependents),
    Deps = [].

find_leaves([], [], _, _).

find_leaves([P|PredicateList], Leaves, Graph, GraphWithDependents):-
    check_instantiated([P, PredicateList, Graph, GraphWithDependents]),
    is_leaf(P, Graph, GraphWithDependents),
    find_leaves(PredicateList, Leaves2, Graph, GraphWithDependents),
    merge_lists(Leaves2, [P], Leaves).

find_leaves([_|PredicateList], Leaves, Graph, GraphWithDependents):-
    check_instantiated([PredicateList, Graph, GraphWithDependents]),
    find_leaves(PredicateList, Leaves, Graph, GraphWithDependents).

is_leaf(P, Graph, GraphWithDependents):-
    check_instantiated([P, Graph, GraphWithDependents]),
    find_all_direct_dependencies(P, D, Graph, GraphWithDependents),
    (
        D = [];
        D = [[]]
    ), !.


%% ------------------------------------------------------------- %%
%%                         Dependencies                          %%
%% ------------------------------------------------------------- %%

% find all groups of connected nodes in a given graph.
% Return list of lists of relations.
list_connected_nodes(Heads, Graph, GraphWithDependents, ConnectedNodes):-
    check_instantiated([Heads, Graph, GraphWithDependents]),
    list_connected_nodes_(Heads, Graph, GraphWithDependents, [], ConnectedNodes).

% return Connected = 1 if Dependency is a direct or indirect dependency of any dependent in Dependents, else 0.
indirect_dependency(Dependents, Dependency, Graph, GraphWithDependents, Connected):-
    check_instantiated([Dependents, Dependency, Graph, GraphWithDependents]),
    length(Dependents, _), % Multiple dependents
    indirect_dependency_multiple(Dependents, Dependency, Graph, [], GraphWithDependents, Connected).

% return Connected = 1 if Dependency is a direct or indirect dependency of Dependent, else 0.
indirect_dependency(Dependent, Dependency, Graph, GraphWithDependents, Connected):-
    check_instantiated([Dependent, Dependency, Graph, GraphWithDependents]),
    indirect_dependency_(Dependent, Dependency, Graph, [], GraphWithDependents, Connected).

% Find direct dependencies of predicate, not(predicate) and not(not(predicate))
find_all_direct_dependencies(Predicate, Dependencies, Graph, GraphWithDependents):-
    check_instantiated([Predicate, Graph, GraphWithDependents]),
    find_all_direct_dependencies_(Predicate, Dependencies, Graph, GraphWithDependents).

% Find direct dependencies of predicate
find_all_exact_direct_dependencies(Predicate, Dependencies, Graph, GraphWithDependents):-
    check_instantiated([Predicate, Graph, GraphWithDependents]),
    find_all_exact_direct_dependencies_(Predicate, Dependencies, Graph, GraphWithDependents).

% Find direct and indirect dependencies of predicate, not(predicate) and not(not(predicate))
find_all_dependencies(Predicate, Dependencies, Graph, GraphWithDependents):-
    check_instantiated([Predicate, Graph, GraphWithDependents]),
    find_all_dependencies_(Predicate, Dependencies, [], Graph, GraphWithDependents).

% Find direct and indirect dependencies of predicate
find_all_exact_dependencies(Predicate, Dependencies, Graph, GraphWithDependents):-
    check_instantiated([Predicate, Graph, GraphWithDependents]),
    find_all_exact_dependencies_(Predicate, [], Dependencies, Graph, GraphWithDependents).

%% ------------------------------------------------------------- %%
%%                          Dependents                           %%
%% ------------------------------------------------------------- %%

% Find direct dependents of predicate, not(predicate) and not(not(predicate))
find_all_direct_dependents(Predicate, Dependents, Graph, GraphWithDependents):-
    check_instantiated([Predicate, Graph, GraphWithDependents]),
    find_all_direct_dependents_(Predicate, Dependents, Graph, GraphWithDependents).

% Find direct dependents of predicate
find_all_exact_direct_dependents(Predicate, Dependents, Graph, GraphWithDependents):-
    check_instantiated([Predicate, Graph, GraphWithDependents]),
    find_all_exact_direct_dependents_(Predicate, [], Dependents, Graph, GraphWithDependents).

% Find direct and indirect dependents of predicate, not(predicate) and not(not(predicate))
find_all_dependents(Predicate, Dependents, Graph, GraphWithDependents):-
    check_instantiated([Predicate, Graph, GraphWithDependents]),
    find_all_dependents_(Predicate, Dependents, [], Graph, GraphWithDependents).

% Find direct and indirect dependents of predicate
find_all_exact_dependents(Predicate, Dependents, Graph, GraphWithDependents):-
    check_instantiated([Predicate, Graph, GraphWithDependents]),
    find_all_exact_dependents_(Predicate, [], Dependents, Graph, GraphWithDependents).

%% ------------------------------------------------------------- %%
%%                     Conversion predicates                     %%
%% ------------------------------------------------------------- %%

%!  rules_to_graph(+Rules:list, +IncludeDependents:int, -SortedRules:list)
%   Given a set of rules in @var{Rules}, returns its reprentation as a
%   graph ([[predicate1, [[clause1], [clause2]]], [predicate2, ...]]) in @var{SortedRules}.
%   If @var{IncludeDependents} = 1, nodes include a reference to their direct predecessors:
%   ([[predicate1, [predecesor1, ...], [[clause1], [clause2]]], [predicate2, ...]]).

rules_to_graph(Rules, 0, Sorted):- % Wrapper for rules_to_graph/4
    check_instantiated([Rules]),
    rules_to_graph_(Rules, [], [], Sorted).

rules_to_graph(Rules, 1, Sorted):- % Wrapper for rules_to_graph/4 with dependents
    check_instantiated([Rules]),
    rules_to_graph_(Rules, [], [], SortedWithoutDependents),
    add_dependents(SortedWithoutDependents, Sorted).

%!  add_dependents(+InitialGraph:list, -FinalGraph:list)
%   Add dependents to a given @var{InitialGraph} without dependent references and return it in @var{FinalGraph}.
add_dependents(Graph, Result):-
    check_instantiated([Graph]),
    add_dependents_(Graph, Graph, Result).

delete_dependents([], []).

% It is a missing predicate (it is removed)
delete_dependents([[_Predicate, _Dependents, [[false]]]|StarGraph], SimpleGraph):-
    check_instantiated([_Predicate, _Dependents, StarGraph]),
    delete_dependents(StarGraph, SimpleGraph).

% It must be replaced
delete_dependents([[Predicate, _Dependents, Descendents]|StarGraph], [[Predicate, Descendents]|SimpleGraph]):-
    check_instantiated([_Predicate, _Dependents, Descendents, StarGraph]),
    delete_dependents(StarGraph, SimpleGraph).

add_value_to_leaf(Graph, Leaf, Value, NewGraph, GraphWithDependents):-
    check_instantiated([Graph, Leaf, Value, GraphWithDependents]),
    add_value_to_leaf_(Graph, Leaf, Value, NewGraph, GraphWithDependents).

%% ------------------------------------------------------------- %%
%%                    Subgraph predicates                        %%
%% ------------------------------------------------------------- %%

% Create a graph given an original graph and the list of predicates
% (module) that represents the contents of the desired graph. The
% module is a subset of the predicates present in the original graph,
% and must be connected.

sub_branch(_, [], _, []).

sub_branch(SubModule, Graph, GraphWithDependents, SubGraph):-
    check_instantiated([SubModule, Graph, GraphWithDependents]),
    sub_branch_(SubModule, Graph, [], GraphWithDependents, SubGraph).

sub_branch_(_SubModule, [], Acc, _GraphWithDependents, SubGraph):-
    reverse(Acc, SubGraph).

sub_branch_(SubModule, [G|Graph], Acc, GraphWithDependents, SubGraph):-
    G = [Predicate|_],
    member(Predicate, SubModule),
    sub_branch_(SubModule, Graph, [G|Acc], GraphWithDependents, SubGraph).
    
sub_branch_(SubModule, [_|Graph], Acc, GraphWithDependents, SubGraph):-
    sub_branch_(SubModule, Graph, Acc, GraphWithDependents, SubGraph).


%% ------------------------------------------------------------- %%
%%                           Path                                %%
%% ------------------------------------------------------------- %%

find_path(Start, End, Graph, GraphWithDependents, Paths):-
    check_instantiated([Start, End, Graph, GraphWithDependents]),
    find_path_(Start, End, [], [], Graph, GraphWithDependents, Paths).
