:- module('dual_rules_for_graphs', [
    generate_dual_for_predicate/4
]).

%% ------------------------------------------------------------- %%
:- use_package(assertions).
:- doc(filetype, module).

:- doc(module, "

This module implements the predicates needed to create dual rules (using a graph format).

").

%% ------------------------------------------------------------- %%

:- use_module('./graph', [find_all_exact_direct_dependents/4]).
:- use_module('../auxiliary_functions/auxiliary', [add_list_level/2, permute/3, avoid_triple/2]).
:- use_module('../auxiliary_functions/aux_negation', [negate/3]).

%% ------------------------------------------------------------- %%

% Generate dual rule for a predicate in a graph
generate_dual_for_predicate(Predicate, Graph, DualRule, 0):-
    member([Predicate, Deps], Graph),
    negate_dependencies(Deps, NegDeps),
    add_list_level(NegDeps, NegDeps1),
    negate([Predicate], [], [NegatedPredicate]),
    DualRule = [NegatedPredicate, NegDeps1].

generate_dual_for_predicate(Predicate, Graph, DualRule, 1):-
    member([Predicate, _, Deps], Graph),
    negate_dependencies(Deps, NegDeps),
    negate([Predicate], [], NegatedPredicate),
    find_all_exact_direct_dependents(NegatedPredicate, Dependents, Graph, 1),
    DualRule = [NegatedPredicate, Dependents, NegDeps].

% Negate body of a predicate

negate_dependencies([], []).
negate_dependencies(Dependencies, NegatedDependencies):-
    negate_dependencies_(Dependencies, [], NegatedDependencies).

negate_dependencies_([], D, Negated):-
    reverse(D, Negated).

negate_dependencies_([D|[]], Negations, NegatedDependencies):-
    D \= [[_|_]|_],
    negate(D, [], NegD),
    permute(NegD, Negations, NegatedDependencies).

negate_dependencies_([D|Dependencies], Negations, NegatedDependencies):-
    negate(D, [], NegD),
    negate_dependencies_(Dependencies, [NegD|Negations], NegatedDependencies).

% replace_in_lists(OldBody, OldLists, NewBody, NewLists)
% Double list
replace_and_permute(_, [], _, []).

replace_and_permute(Old, [L|List], Content, NewList):-
    [L|List] = [[_|_]|_],
    replace_and_permute(Old, L, Content, NewL),
    replace_and_permute(Old, List, Content, NewList2),
    avoid_triple(NewL, NewL1),
    NewList = [NewL1|NewList2].

% Simple list
replace_and_permute(Old, List, Content, NewList):-
    member(Old, List),
    select(Old, List, OtherContents1),
    add_list_level(OtherContents1, OtherContents),
    permute(Content, OtherContents, NewList).

replace_and_permute(_, List, _, List).
