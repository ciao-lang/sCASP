:- module(print, [
    print_iterative/1,
    print_dcg/1
]).

:- op(800, fy, 'not not').

print_iterative(Program):-
    clauses_iterative(Program, '', FormattedProgram),
    display(FormattedProgram).

% Receives a list, returns an atom
clauses_iterative([], FormattedProgram, FormattedProgram).

clauses_iterative([[Predicate, Bodies]|Program], Acc, FormattedProgram):-
    clause_iterative(Predicate, Bodies, '', FormattedClauses),
    atom_concat(Acc, FormattedClauses, Acc2),
    clauses_iterative(Program, Acc2, FormattedProgram).

clause_iterative(_Predicate,[], FormattedClause, FormattedClause).

% fact
clause_iterative(Predicate, [[]], Acc, FormattedClause):-
    pred_iterative(Predicate, Head),
    atom_concat(Head, '.\n', FormattedClause1),
    atom_concat(Acc, FormattedClause1, FormattedClause).

clause_iterative(Predicate,[Body|Bodies], Acc, FormattedClauses):-
    atom_concat(Predicate, ':-\n     ', Head),
    body_iterative(Body, '', FormattedBody),
    atom_concat(Head, FormattedBody, FormattedClause),
    atom_concat(Acc, FormattedClause, Acc2),
    clause_iterative(Predicate, Bodies, Acc2, FormattedClauses), !.

body_iterative([], FormattedBody, FormattedBody).

body_iterative([Predicate|[]], Acc, FormattedBody):-
    pred_iterative(Predicate, FormattedPredicate),
    atom_concat(FormattedPredicate, '.\n', FormattedBody1),
    atom_concat(Acc, FormattedBody1, FormattedBody).

body_iterative([Predicate|Predicates], Acc, FormattedBody):-
    pred_iterative(Predicate, FormattedPredicate),
    atom_concat(FormattedPredicate, ',', FormattedPredicate1),
    atom_concat(Acc, FormattedPredicate1, Acc2),
    body_iterative(Predicates, Acc2, FormattedBody), !.

pred_iterative(not(not(Predicate)), FormattedPredicate):-
    atom_concat('not not ', Predicate, FormattedPredicate), !.

pred_iterative(not(Predicate), FormattedPredicate):-
    atom_concat('not ', Predicate, FormattedPredicate), !.

pred_iterative(Predicate, Predicate).

print_dcg(Program):-
    phrase(clauses(Program), A),
    atom_chars(FormattedProgram, A),
    display(FormattedProgram).

% Receives a list, returns a string
clauses([Clause|Clauses])-->
    clause_dcg(Clause),
    !,
    clauses(Clauses).

clauses([])-->
    [].

% fact
clause_dcg([Head, [[]]])-->
    [], pred(Head), ['.', '\n'].

clause_dcg([Head, [[]|Bs]])-->
    pred(Head), ['.','\n'], clause_dcg([Head, Bs]), !.

clause_dcg([Head, [Body|Bs]])-->
    pred(Head), [' ',':','-','\n',' ',' ',' ',' ',' '], body(Body), ['.','\n'], clause_dcg([Head, Bs]), !.

clause_dcg([Head, []])-->
    [], {atom(Head)}, !.

body([Predicate])-->
    pred(Predicate).

body([Predicate|Predicates])-->
    pred(Predicate), !,
    [',', '\n',' ',' ',' ',' ',' '], body(Predicates).

pred([Predicate])-->
    pred(Predicate), !.

pred(not(not(Predicate)))-->
    {atom_chars(Predicate, [C|Cs])}, [n,o,t,' ',n,o,t,' '], [C|Cs], !.

pred(not(Predicate))-->
    {atom_chars(Predicate, [C|Cs])}, [n,o,t,' '], [C|Cs], !.

pred(Predicate)-->
    {atom_chars(Predicate,[C|Cs])}, [C|Cs].
