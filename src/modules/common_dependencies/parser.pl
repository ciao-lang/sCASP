:- module(parser, [
    program_to_graph/2
]).

:- reexport('../../sasp/output', [pr_rule/2]).
:- use_module('./auxiliary_functions/auxiliary', [merge_lists/3]).
:- use_module('./graph/graph', [rules_to_graph/3]).

program_to_graph(Graph, IncludeDependents):-
    % obtain all rules
    findall([Head,Body], pr_rule(Head,Body), Rules),
    % separate dual rules, global constraints and nmr checks
    separate_rule_type(Rules, Original, _Dual, _Others),
    % transform rules into a graph
    rules_to_graph(Original, IncludeDependents, Graph).

separate_rule_type([], [], [], []).

separate_rule_type(Program, Rules, DualRules, Others) :-
    separate_rule_type_(Program, Rules, [], DualRules, [], Others, []).

separate_rule_type_([], Rules, AccR, DualRules, AccD, Others, AccO):-
    reverse(AccR, Rules),
    reverse(AccD, DualRules),
    reverse(AccO, Others),
    !.

% dual rule
separate_rule_type_([P|Program], Rules, AccR, DualRules, AccD, Others, AccO):-
    P = [PName|_],
    PName =.. [not|_],
    separate_rule_type_(Program, Rules, AccR, DualRules, [P|AccD], Others, AccO),
    !.

% other type (o_nmr_check and global_constraints)
separate_rule_type_([P|Program], Rules, AccR, DualRules, AccD, Others, AccO):-
    (P = [global_constraints|_];
    P = [o_nmr_check|_]),
    separate_rule_type_(Program, Rules, AccR, DualRules, AccD, Others, [P|AccO]).

% original rule
separate_rule_type_([P|Program], Rules, AccR, DualRules, AccD, Others, AccO):-
    separate_rule_type_(Program, Rules, [P|AccR], DualRules, AccD, Others, AccO).

create_predicate_list([], []).

create_predicate_list([[Predicate, Body]|Graph], PredicateList):-
    create_predicate_list_(Body, PredicateListBody),
    merge_lists([Predicate], PredicateListBody, PredicateList1),
    create_predicate_list(Graph, PredicateList2),
    merge_lists(PredicateList1, PredicateList2, PredicateList),!.

create_predicate_list_([], []).

create_predicate_list_([Clause|Clauses], PredicateList):-
    create_predicate_list_(Clauses, PredicateList1),
    merge_lists(Clause, PredicateList1, PredicateList).
