:- module(loops, [
    find_loops/4,
    find_loops_predicate/4,
    find_loops_modules/4,
    filter_loops/3
]).

%% ------------------------------------------------------------- %%
:- use_package(assertions).
:- doc(filetype, module).

:- doc(module, "

This module identifies loops in a module or program.

").

%% ------------------------------------------------------------- %%

% Dependencies
:- use_module('../auxiliary_functions/auxiliary', [
    merge_lists/3, repeated_elements/3,
    select_non_repeated_elements/3, destructive_append/3,
    append_non_repeated_list/3]).
:- use_module('../graph/graph', [find_path/5]).

%% ------------------------------------------------------------- %%
%% ------------------------------------------------------------- %%
%%                          Predicates                           %%
%% ------------------------------------------------------------- %%
%% ------------------------------------------------------------- %%

%% ------------------------------------------------------------- %%
%%                         Loop creation                         %%
%% ------------------------------------------------------------- %%

find_loops_predicate(Predicate, Graph, GraphWithDependents, Loops):-
    find_path(Predicate, Predicate, Graph, GraphWithDependents, Loops).

% Identify loops for multiple modules
find_loops_modules([], _, _, []).

find_loops_modules([M|Modules], Graph, GraphWithDependents, Loops):-
    nonvar(Graph),
    find_loops(M, Graph, GraphWithDependents, Loops1),
    find_loops_modules(Modules, Graph, GraphWithDependents, Loops2),
    Loops = [Loops1|Loops2].

find_loops(Module, Graph, GraphWithDependents, Loops):-
    find_loops_(Module, Graph, GraphWithDependents, [], Loops).

find_loops_([], _Graph, _GraphWithDependents, Loops, Loops).

find_loops_([Predicate|Module], Graph, GraphWithDependents, Acc, Loops):-
    find_loops_predicate(Predicate, Graph, GraphWithDependents, LoopPredicate),
    append_non_repeated_list(LoopPredicate, Acc, NewAcc),
    find_loops_(Module, Graph, GraphWithDependents, NewAcc, Loops).

filter_loops(Loops, EvenLoops, OddLoops):-
    filter_loops_(Loops, [], EvenLoops, [], OddLoops).

filter_loops_([], Even, Even, Odd, Odd).

filter_loops_([L|Loops], EvenAcc, Even, OddAcc, Odd):-
    count_negations(L, 0, Num),
    ((
        even_num(Num),
        filter_loops_(Loops, [L|EvenAcc], Even, OddAcc, Odd)
    );(
        filter_loops_(Loops, EvenAcc, Even, [L|OddAcc], Odd)
    )), !.

count_negations([], Acc, Acc).

count_negations([P|Pred], Acc, Num_negs):-
    P = not(not(_)),
    Acc2 is Acc + 2,
    count_negations(Pred, Acc2, Num_negs).

count_negations([P|Pred], Acc, Num_negs):-
    P = not(_),
    Acc2 is Acc + 1,
    count_negations(Pred, Acc2, Num_negs).

count_negations([_P|Pred], Acc, Num_negs):-
    count_negations(Pred, Acc, Num_negs).

even_num(Num) :-
    Num mod 2 =:= 0, !.
