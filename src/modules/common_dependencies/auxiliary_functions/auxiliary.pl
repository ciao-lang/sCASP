:- module(auxiliary, [
    add_list_level/2,
    appear/2,
    append_non_repeated_list/3,
    avoid_doble/2,
    avoid_triple/2,
    before_after_element/4,
    bigger_than/2,
    check_instantiated/1,
    clean_repeated_lists/2,
    combine/3,
    delete_all_occurrences/3,
    delete_first_occurrence/3,
    delete_in_lists_of_lists/3,
    delete_repeated_elements/2,
    destructive_append/3,
    double_list/1,
    find_rules/5,
    flatten_list/2,
    get_predicate_heads/2,
    has_lists/1,
    head/2,
    heads/2,
    last_element/2,
    lesser_than/2,
    max/2,
    merge_lists/3,
    min/2,
    permute/3,
    present_in_both/3,
    restore_facts_missing_predicates/2,
    repeated_elements/3,
    replace_in_lists/4,
    replace_keeping_order/4,
    replace_all_keeping_order/4,
    same_contents/2,
    select_non_repeated_elements/3,
    select_non_repeated_elements_multiple_lists/3,
    select_non_repeated_elements_beta/3,
    statement_appear_in_module/2
]).

%% ------------------------------------------------------------- %%%% ------------------------------------------------------------- %%

:- use_package(assertions).
:- doc(filetype, module).

:- doc(module, "

This module implements common utilities.

").

%% ------------------------------------------------------------- %%

:- use_module(library(system)).

%% ------------------------------------------------------------- %%

% Append a list if the elements in them are not already present in another
% If already present, keep the longer list
append_non_repeated_list([], ListOfLists, ListOfLists) :- !.
append_non_repeated_list(NewList, [], [NewList]) :- !.
append_non_repeated_list(NewList, [L|ListOfLists], NewListOfLists):-
    repeated_elements(NewList, L, []), % Not the same list
    append_non_repeated_list(NewList, ListOfLists, NewListOfLists1),
    merge_lists(NewListOfLists1, [L|ListOfLists], NewListOfLists), !.

append_non_repeated_list(NewList, [L|ListOfLists], NewListOfLists):-
    repeated_elements(NewList, L, Repeated), % Same list
    select_longer_list(NewList, L, Repeated, LongerList),
    merge_lists([LongerList], ListOfLists, NewListOfLists).

% Select the longer list between two lists
select_longer_list(New, Old, RepeatedElements, LongerList):-
    select_non_repeated_elements_beta(New, RepeatedElements, NonRep1),
    select_non_repeated_elements_beta(Old, RepeatedElements, NonRep2),
    destructive_append(NonRep1, NonRep2, NonRep),
    destructive_append(RepeatedElements, NonRep, LongerList), !.

destructive_append(List1, [], List1):- !.

destructive_append([], List2, List2) :- !.

destructive_append(List1, List2, FinalList):-
    destructive_append_(List1, List2, FinalList).

destructive_append_([], FinalList, FinalList) :- !.

destructive_append_([L|List1], List2, [L|FinalList]):-
    destructive_append_(List1, List2, FinalList), !.

destructive_append_(L, List2, [L|List2]):-
    \+list(L, _),
    list(List2), !.

destructive_append_(L, L2, [L, L2]).

before_after_element(Goal, BeforeElement, [ActualNode|CompleteGraph], AfterElement):-
    before_after_element_(Goal, ActualNode, [], BeforeElement, CompleteGraph, AfterElement), !.

before_after_element_(Goal, Node, Acc, BeforeElement, AfterElement, AfterElement):-
    Node = Goal,
    reverse(Acc, BeforeElement), !.

before_after_element_(Goal, Node, Acc, BeforeElement, [N|Graph], AfterElement):-
    Acc2 = [Node|Acc],
    before_after_element_(Goal, N, Acc2, BeforeElement, Graph, AfterElement).

check_instantiated([]).
check_instantiated([V|Vars]):-
    nonvar(V),
    check_instantiated(Vars), !.

merge_lists(List, [], List) :- !.
merge_lists([], List, List) :- !.

merge_lists(List1, [L|List2], FinalList):-
    member(L, List1),
    merge_lists(List1, List2, FinalList), !.

merge_lists(List1, [L|List2], FinalList):-
    destructive_append(List1, [L], NewList1),
    merge_lists(NewList1, List2, FinalList).

same_contents([], []) :- !.

same_contents(List1, [Element|List2]):-
    List1 \= [],
    [Element|List2] \= [],
    select(Element, List1, List1_1),
    same_contents(List1_1, List2).

repeated_elements(List1, List2, Repeated):-
    repeated_elements_(List1, List2, [], Repeated), !.

repeated_elements_([], _, Repeated, Repeated) :- !.

repeated_elements_(_, [], Repeated, Repeated) :- !.

repeated_elements_(List1, [L|List2], Acc, Repeated):-
    member(L, List1),
    merge_lists(Acc, [L], Acc2),
    repeated_elements_(List1, List2, Acc2, Repeated), !.

repeated_elements_(List1, [L|List2], Acc, Repeated):-
    \+member(L, List1),
    repeated_elements_(List1, List2, Acc, Repeated).


delete_repeated_elements([],[]) :- !.
delete_repeated_elements(List, NewList):-
    delete_repeated_elements_(List, [], NewList).

delete_repeated_elements_([], Acc, Acc) :- !.

delete_repeated_elements_([L|List], Acc, NewList):-
    member(L, Acc),
    delete_repeated_elements_(List, Acc, NewList), !.

delete_repeated_elements_([L|List], Acc, NewList):-
    destructive_append(Acc, [L], NewAcc),
    delete_repeated_elements_(List, NewAcc, NewList).

% Rebuild List 1 with only the elements present in List, using the structure from List 1.
present_in_both([], _, []) :- !.
present_in_both(_, [], []) :- !.
present_in_both([L|List1], List2, NewList):-
    present_in_both_(L, List2, NewNode),
    present_in_both(List1, List2, NewNodes),
    NewList = [NewNode|NewNodes].

present_in_both_([], _, []) :- !.
present_in_both_([N|Node], List2, NewNode):-
    member(N, List2),
    present_in_both_(Node, List2, NewNode2),
    NewNode = [N|NewNode2], !.

present_in_both_([N|Node], List2, NewNode):-
    present_in_both_(N, List2, NewNode1),
    present_in_both_(Node, List2, NewNode2),
    destructive_append(NewNode1, NewNode2, NewNode), !.

present_in_both_([_|Node], List2, NewNode):-
    present_in_both_(Node, List2, NewNode).

% replace_in_lists(OldBody, OldLists, NewBody, NewLists)
% Double list
replace_in_lists(_, [], _, []) :- !.

replace_in_lists(Old, [L|List], Content, NewList):-
    [L|List] = [[_|_]|_],
    replace_in_lists(Old, L, Content, NewL),
    replace_in_lists(Old, List, Content, NewList2),
    NewList = [NewL|NewList2], !.

% Simple list

replace_in_lists(Old, List, Content, NewList):-
    member(Old, List),
    select(Old, List, NewList1),
    destructive_append(NewList1, Content, NewList), !.

replace_in_lists(_, List, _, List).

clean_repeated_lists([P],Clean):-
    P = [_],
    clean_repeated_lists(P, Clean), !.

clean_repeated_lists([P],Clean):-
    length(P,N),
    N>1,
    add_list_level(P,Clean), !.

clean_repeated_lists(Clean,[Clean]).

add_list_level([],[]) :- !.

add_list_level([P|Ps], Branched):-
    avoid_doble(P, PB),
    add_list_level(Ps, PsB),
    Branched = [PB|PsB], !.

add_list_level(P, [P]).

% Combine vs permute
% Combine
% [xy] x [ab] = [xab][yab]
% [xy] x [ab][cd] =  [xab][yab] x [cd] = [xabcd][yabcd]
% Permute
% [xy] x [ab] = [xa][xb][ya][yb]
% [xy] x [ab][cd] =  [xa][xb][ya][yb] x [cd] = [xac][xad][xbc][xbd][yac][yad][ybc][ybd]

permute([],A,A) :- !.

permute(A,[],A) :- !.
    
% [xy] x [ab][cd] =  [xa][xb][ya][yb] x [cd] = [xac][xad][xbc][xbd][yac][yad][ybc][ybd]
permute(X, [Y|Ys], Permutation):-
    Y = [_|_],
    add_list_level(X,X1),
    add_list_level(Y,Y1),
    permute_(X1, Y1, X2),
    add_list_level(X2, X3),
    (
        (Ys = [],
        X3 = Permutation)
    ;
        (permute(X3, Ys, Permutation))
    ), !.

% [xy] x [ab] =  [xa][xb][ya][yb]
permute(X, [Y|Ys], Permutation):-
    permute_(X, [Y|Ys], Permutation).

permute_([],A,A) :- !.

permute_(A,[],A) :- !.

permute_(X, [Y|Ys], Permutation):-
    add_list_level(Y, Y1),
    combine(X, Y1, X2),
    (
        (Ys = [],
        Permutation = X2)
    ;
        (permute_(X, Ys, Permutation1),
        destructive_append(X2, Permutation1, Permutation))
    ).

combine([], _, []) :- !.
combine(X,[],X) :- !.

% [[x][y]] x [[a][b]] = [xa][xb][ya][yb]
combine([X|Xs], Y, Combination):-
    combine_(X, Y, CombinationX),
    combine(Xs, Y, CombinationXs),
    Combination = [CombinationX|CombinationXs], !.

combine(X, Y, Combination):-
    X \= [_|_],
    combine([X], Y, Combination), !.

combine(X, Y, Combination):-
    Y \= [_|_],
    combine(X, [Y], Combination).

combine_(_, [],[]) :- !.
combine_([],X,X) :- !.
combine_(X, [Y|Ys], Combination):-
    avoid_doble(X,X1),
    avoid_doble(Y,Y1),
    destructive_append(X1,Y1,XY),
    combine_(X1,Ys, XYs),
    merge_lists(XY, XYs, Combination), !.

% [[xy]] x [[ab]] = [xyab]
combine_(X, Y, Combination):-
    avoid_doble(X,X1),
    avoid_doble(Y,Y1),
    destructive_append(X1,Y1,Combination).

delete_first_occurrence([], List, List) :- !.
delete_first_occurrence(_, [], []) :- !.
delete_first_occurrence([Element|Elements_to_delete], List, NewList) :-
    ( select(Element, List, List1) ;
        List = List1 ),
    delete_first_occurrence(Elements_to_delete, List1, NewList), !.

delete_first_occurrence(Element, List, NewList) :-
    delete_first_occurrence([Element], List, NewList).

delete_all_occurrences(_, [], []) :- !.

delete_all_occurrences(Element, [O|OldList], NewList):-
    Element \= O,
    delete_all_occurrences(Element, OldList, NewList1),
    NewList = [O|NewList1], !.

delete_all_occurrences(Element, [Element|OldList], NewList):-
    delete_all_occurrences(Element, OldList, NewList).

delete_in_lists_of_lists(_Element, [], []) :- !.

delete_in_lists_of_lists(Element, [List|Lists], NewList):-
    double_list(List),
    delete_in_lists_of_lists(Element, List, NewList1),
    delete_in_lists_of_lists(Element, Lists, NewLists),
    NewList = [NewList1|NewLists], !.

delete_in_lists_of_lists(Element, [List|Lists], NewList):-
    delete_all_occurrences(Element, List, NewList1),
    delete_in_lists_of_lists(Element, Lists, NewLists),
    NewList = [NewList1|NewLists], !.

delete_in_lists_of_lists(Element, [List|Lists], NewList):-
    \+length(List, _),
    delete_all_occurrences(Element, [List], NewList1),
    delete_in_lists_of_lists(Element, Lists, NewLists),
    destructive_append(NewList1, NewLists, NewList).

last_element([L|[]], L).
last_element([_|List], Last):-
    last_element(List, Last).

flatten_list([L|List], Flat):-
    flatten_list(L, Flat1),
    flatten_list(List, Flat2),
    avoid_doble(Flat1, FFlat1),
    avoid_doble(Flat2, FFlat2),
    destructive_append(FFlat1, FFlat2, Flat), !.

flatten_list(L, L).

has_lists([L|_]):-
    length(L, N),
    N \= 0.

has_lists([_|List]):-
    has_lists(List). 

avoid_doble(S,S):-
    length(S,_).
  
avoid_doble(S, [S]):-
\+length(S,_).

avoid_triple(X,X):-
    X = [[_|_]|_].

avoid_triple(X,[X]).

double_list(L):-
    L = [[_|_]|_].

double_list([[_|_]|List]):-
    double_list(List).

double_list([L|List]):-
    length(L, _),
    double_list(List).

double_list(L):-
    L = [[_|_]|_].

double_list([_L|List]):-
    double_list(List).

lesser_than(_, []).

lesser_than(X, [V|Values]):-
    X < V,
    lesser_than(X, Values).

bigger_than(_, []).

bigger_than(X, [V|Values]):-
    X > V,
    bigger_than(X, Values).
%%                  Get head of statement/s                 %%

heads([], []).

heads([Rules], Heads):-
    heads(Rules, Heads).

heads([R|Rules], Heads):-
   heads(R, Heads1),
   heads(Rules, Heads2),
   destructive_append(Heads1, Heads2, Heads).

heads([R|Rules], Heads):-
    head(R, Heads1),
    heads(Rules, Heads2),
    destructive_append(Heads1, Heads2, Heads).

heads(Rules, Heads):-
    head(Rules, Heads).

head([Rule], Head):-
    head(Rule, Head).

head(Rule, []):-
    Rule =.. [variable,_].

head(Rule, []):-
    Rule =.. [number,_].

head(Rule, []):-
    Rule =.. [constant,_].

head(Rule, Heads):-
    Rule =.. [not, Statements],
    heads([Statements], Heads).

head(Rule, Heads):-
    Rule =.. [classicalNot(Statements)],
    heads(Statements, Heads).

head(Rule, Heads):- % assignment and operations
    Rule = (Statements1, _, Statements2),
    heads([Statements1], Heads1),
    heads([Statements2], Heads2),
    destructive_append(Heads1, Heads2, Heads).

head(Rule, Head):-
    Rule =.. [_,Name,_],
    Head = [Name].

head(_, []).

% Get a list of predicates in a program
get_predicate_heads(Program, Heads):-
    get_predicate_heads_(Program, [], Heads1),
    delete_all_occurrences(false, Heads1, Heads2),
    delete_all_occurrences(true, Heads2, Heads), !.

get_predicate_heads_([], Heads, Heads) :- !.

get_predicate_heads_([[Predicate, _Dependents, Descendents]|Program], Acc, Heads):-
    merge_lists(Acc, [Predicate], Acc1),
    flatten_list(Descendents, AllDescendents),
    merge_lists(Acc1, AllDescendents, Acc3),
    get_predicate_heads_(Program, Acc3, Heads), !.

get_predicate_heads_([[Predicate, Descendents]|Program], Acc, Heads):-
    merge_lists(Acc, [Predicate], Acc1),
    flatten_list(Descendents, AllDescendents),
    merge_lists(Acc1, AllDescendents, Acc3),
    get_predicate_heads_(Program, Acc3, Heads).

% Replace first occurence maintaining order
replace_keeping_order(OriginalList, ItemToReplace, NewItem, NewList):-
    replace_keeping_order_(OriginalList, ItemToReplace, [], NewItem, NewList).

replace_keeping_order_([], _, NewList, _, NewList) :- !.

% Item = ItemToReplace
replace_keeping_order_([ItemToReplace|OriginalList], ItemToReplace, AlreadySeen, NewItem, NewList):-
    reverse(AlreadySeen, Prev),
    merge_lists(Prev, [NewItem|OriginalList], NewList), !.

% Item != ItemToReplace
replace_keeping_order_([Item|OriginalList], ItemToReplace, AlreadySeen, NewItem, NewList):-
    replace_keeping_order_(OriginalList, ItemToReplace, [Item|AlreadySeen], NewItem, NewList).

% Replace all occurences maintaining order
replace_all_keeping_order([], _ItemToReplace, _NewItem, []) :- !.

replace_all_keeping_order(OriginalList, ItemToReplace, NewItem, NewList):-
    replace_all_keeping_order_(OriginalList, ItemToReplace, [], NewItem, NewList).

replace_all_keeping_order_([], _, AlreadySeen, _, NewList):-
    reverse(AlreadySeen, NewList), !.

% Item = ItemToReplace
replace_all_keeping_order_([ItemToReplace|OriginalList], ItemToReplace, AlreadySeen, NewItem, NewList):-
    replace_all_keeping_order_(OriginalList, ItemToReplace, [NewItem|AlreadySeen], NewItem, NewList), !.

% Item != ItemToReplace
replace_all_keeping_order_([Item|OriginalList], ItemToReplace, AlreadySeen, NewItem, NewList):-
    replace_all_keeping_order_(OriginalList, ItemToReplace, [Item|AlreadySeen], NewItem, NewList).

find_rules(_, _, [], [], []). % Empty input
find_rules(_, _, [], Rules, Rules). % Exit

find_rules(Head, Length, [S|Statements], Acc, Rules):-
    S =.. [.,Head1,Body], % get head and body
    Head1 =.. [Head|X],
    length(X, Length),
    destructive_append(Acc, Body, NewAcc),
    find_rules(Head, Length, Statements, NewAcc, Rules).

find_rules(Head, L, [_|Statements], Acc, Rules):-
    find_rules(Head, L, Statements, Acc, Rules).

% Statement appears in module
statement_appear_in_module(Statement, DependencyList):-
    Statement =.. [rule, Head, _],
    head(Head, [Name]),
    member(Name, DependencyList).

% predicate name appears in list of lists
appear(Statement, [List|Lists]):-
    member(Statement, List);
    appear(Statement, Lists), !.

appear(Statement, List):-
    member(Statement, List).

select_non_repeated_elements_multiple_lists([], [], []).

select_non_repeated_elements_multiple_lists([], Unique, Unique).

select_non_repeated_elements_multiple_lists(Unique, [], Unique).

select_non_repeated_elements_multiple_lists([L1|Lists1], Lists2, Unique):-
    select_non_repeated_elements_multiple_lists_1_n(L1, Lists2, [], Unique1),
    select_non_repeated_elements_multiple_lists(Lists1, Unique1, Unique).

select_non_repeated_elements_multiple_lists_1_n(_, [], Unique, Unique).

select_non_repeated_elements_multiple_lists_1_n(L1, [L2|_Lists2], Acc, Unique):-
    select_non_repeated_elements(L1, L2, []),
    Acc = Unique.

select_non_repeated_elements_multiple_lists_1_n(L1, [L2|Lists2], Acc, Unique):-
    select_non_repeated_elements_multiple_lists_1_n(L1, Lists2, [L2|Acc], Unique).

select_non_repeated_elements([], A, A).
select_non_repeated_elements(A, [], A).
select_non_repeated_elements(A, B, C):-
    A \= [],
    B \= [],
    length(A,LA),
    length(B,LB),
    (   LA > LB,
        select_non_repeated_elements_(A, B, [], C)
    );(
        select_non_repeated_elements_(B, A, [], C)
    ), !.

select_non_repeated_elements_([], _, A, A).

select_non_repeated_elements_(A, [], A, A).

select_non_repeated_elements_([St|AllSt], List, Acc, FilteredList):-
    member(St, List),
    select_non_repeated_elements_(AllSt, List, Acc, FilteredList).

select_non_repeated_elements_([St|AllSt], List, Acc, FilteredList):-
    destructive_append(Acc, [St], NewAcc),
    select_non_repeated_elements_(AllSt, List, NewAcc, FilteredList).

select_non_repeated_elements_beta([], _, []) :- !.

select_non_repeated_elements_beta([St|AllSt], List, FilteredList):-
    member(St, List),
    select_non_repeated_elements_beta(AllSt, List, FilteredList), !.

select_non_repeated_elements_beta([St|AllSt], List, FilteredList):-
    select_non_repeated_elements_beta(AllSt, List, FilteredList2),
    FilteredList = [St|FilteredList2].

min([], []).

min([N|Numbers], Min):-
    min_(Numbers, N, Min).

min_([], Min, Min).
min_([N|Numbers], ActualMin, Min):-
    N<ActualMin,
    min_(Numbers, N, Min).

min_([_|Numbers], ActualMin, Min):-
    min_(Numbers, ActualMin, Min).

max([], []).

max([N|Numbers], Max):-
    max_(Numbers, N, Max).

max_([], Max, Max).
max_([N|Numbers], ActualMax, Max):-
    N>ActualMax,
    max_(Numbers, N, Max).

max_([_|Numbers], ActualMax, Max):-
    max_(Numbers, ActualMax, Max).

restore_facts_missing_predicates(Program, RestoredProgram):-
    restore_facts_missing_predicates_([], Program, RestoredProgram).

restore_facts_missing_predicates_([], [], []) :- !.

restore_facts_missing_predicates_(Program, [], RestoredProgram):-
    reverse(Program, RestoredProgram), !.

restore_facts_missing_predicates_(Program, [[_Predicate, Clauses]|Rest], RestoredProgram):-
    restore_facts_missing_predicates_clauses([], Clauses, [0]),
    restore_facts_missing_predicates_(Program, Rest, RestoredProgram).

restore_facts_missing_predicates_(Program, [[Predicate, Clauses]|Rest], RestoredProgram):-
    restore_facts_missing_predicates_clauses([], Clauses, RestoredClauses),
    restore_facts_missing_predicates_([[Predicate, RestoredClauses]|Program], Rest, RestoredProgram).

restore_facts_missing_predicates_clauses([], [], []).

restore_facts_missing_predicates_clauses(Clauses, [], [0]):-
    delete_missing_predicates([], Clauses, []).

restore_facts_missing_predicates_clauses(Clauses, [], RestoredClauses):-
    delete_missing_predicates([], Clauses, FilteredClauses),
    delete_repeated_elements(FilteredClauses, UniqueClauses),
    reverse(UniqueClauses, RestoredClauses).

restore_facts_missing_predicates_clauses(Clauses, [Clause|Rest], RestoredClauses):-
    restore_facts_missing_predicates_clause([], Clause, ModifiedClause),
    restore_facts_missing_predicates_clauses([ModifiedClause|Clauses], Rest, RestoredClauses).

delete_missing_predicates(Clauses, [], FilteredClauses):-
    reverse(Clauses, FilteredClauses).

delete_missing_predicates(Clauses, [], FilteredClauses):-
    length(Clauses,1),
    FilteredClauses = Clauses.

delete_missing_predicates(Clauses, [[0]|Rest], FilteredClauses):-
    delete_missing_predicates(Clauses, Rest, FilteredClauses).

delete_missing_predicates(Clauses, [Clause|Rest], FilteredClauses):-
    delete_missing_predicates([Clause|Clauses], Rest, FilteredClauses).

restore_facts_missing_predicates_clause([], [], []).
restore_facts_missing_predicates_clause(Clause, [], ModifiedClause):-
    reverse(Clause, ModifiedClause).

restore_facts_missing_predicates_clause(Predicates, [true|Rest], Restored):-
    restore_facts_missing_predicates_clause(Predicates, Rest, Restored).

restore_facts_missing_predicates_clause(_Predicates, [false|_Rest], [0]).

restore_facts_missing_predicates_clause(Predicates, [Predicate|Rest], Restored):-
    restore_facts_missing_predicates_clause([Predicate|Predicates], Rest, Restored).