:- module(aux_negation, [
    negate/3,
    negate_head/2,
    head_neg/2,
    heads_neg/2,
    negate_pred/2]).

%% ------------------------------------------------------------- %%

:- use_package(assertions).
:- doc(filetype, module).

:- doc(module, "

This module implements utilities to negate predicates.

").

%% ------------------------------------------------------------- %%

:- use_module('./auxiliary', [avoid_doble/2, destructive_append/3]).

%% ------------------------------------------------------------- %%

%%                      Individual negation                      %%
negate_pred([],[]).

% Comparations
negate_pred((L, =, R), (L, \=, R)).
negate_pred((L, \=, R), (L, =, R)).
negate_pred((L, =\=, R), (L, =:=, R)).
negate_pred((L, =:=, R), (L, =\=, R)).
negate_pred((L, >, R), (L, =<, R)).
negate_pred((L, <, R), (L, >=, R)).
negate_pred((L, >=, R), (L, <, R)).
negate_pred((L, =<, R), (L, >, R)).

negate_pred((L, #=, R), (L, #<>, R)).
negate_pred((L, #<>, R), (L, #=, R)).
negate_pred((L, #>, R), (L, #=<, R)).
negate_pred((L, #<, R), (L, #>=, R)).
negate_pred((L, #>=, R), (L, #<, R)).
negate_pred((L, #=<, R), (L, #>, R)).

negate_pred((L, @>, R), (L, @=<, R)).
negate_pred((L, @<, R), (L, @>=, R)).
negate_pred((L, @>=, R), (L, @<, R)).
negate_pred((L, @=<, R), (L, @>, R)).
negate_pred(true, false).
negate_pred(false, true).
% Todo...

 % Case not not p(X) --> not p(X)
 % not(not(X))
negate_pred(not(not(X)), not(X)).

% Case not p(X) -->  p(X)
negate_pred(Predicate, X):- 
    Predicate =.. [not, X].

 % Case p(X) --> not p(X)
negate_pred(Predicate,not(Predicate)):-
    Predicate =.. [_,Name,_],
    atom(Name).

negate_pred(Head, NegatedHead):-
    negate_head(Head, NegatedHead).

negate([],Acc, Negations):-
    reverse(Acc,Negations), !.

negate([P|Predicates],Acc, Negations):-
    negate_pred(P, N1),
    negate(Predicates, [N1|Acc], Negations).

negate([P|Predicates],Acc, Negations):-
    negate(P,Acc,N1),
    negate(Predicates, [N1|Acc], Negations).

negate_head(Predicate, X):-
    Predicate = not(X), !.

negate_head(Predicate, not(Predicate)):- !.

heads_neg([], []).

heads_neg([Rules], Heads):- !;
    heads_neg(Rules, Head),
    avoid_doble(Head, Heads).

heads_neg([R|Rules], Heads):- !;
    head_neg(R, Heads1),
    heads_neg(Rules, Heads2),
    destructive_append(Heads1, Heads2, Heads).

heads_neg(Rules, Heads):- !,
    head_neg(Rules, Head),
    avoid_doble(Head, Heads).

head_neg(not(not((S, _))), [not(not((S)))]):- !.

head_neg(not(S), Head):-
    heads_neg(S, H),
    ((
        length(H,1),
        negate(H, [], [Head]) 
    );
        negate(H, [], Head)
    ;
        negate([H], [], [Head])
    ).

head_neg(not(S), not(Head)):-
    heads_neg(S, Head).

head_neg(Head, Head):-
    atom(Head).

head_neg([Rule], Head):-
    head_neg(Rule, Head).

head_neg(Rule, []):-
    Rule =.. [variable,_].

head_neg(Rule, []):-
    Rule =.. [number,_].

head_neg(Rule, []):-
    Rule =.. [constant,_].

head_neg(Rule, NegHeads):-
    Rule =.. [not, Statements],
    heads_neg([Statements], Heads),
    change_neg(Heads, NegHeads).

head_neg(Rule, NegHeads):-
    Rule =.. [classicalNot(Statements)],
    heads_neg(Statements, Heads),
    change_neg(Heads, NegHeads).

head_neg(Rule, Heads):- % assignment and operations
    Rule = (Statements1, _, Statements2),
    heads_neg([Statements1], Heads1),
    heads_neg([Statements2], Heads2),
    destructive_append(Heads1, Heads2, Heads).

head_neg(Rule, Head):-
    Rule =.. [_,Name,_],
    Head = [Name].

head_neg(_, []).

change_neg([],[]).
change_neg(not(P), P).
change_neg(P, not(P)):-
    atom(P).
change_neg([P|Ps], [N|Ns]):-
    change_neg(P, N),
    change_neg(Ps, Ns).