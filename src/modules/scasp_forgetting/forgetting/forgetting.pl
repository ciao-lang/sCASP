:- module(forgetting, 
[
    f_casp/4
]).

%% ------------------------------------------------------------- %%
:- use_package(assertions).
:- doc(filetype, module).

:- doc(module, "
This module implements the '--forget' flag, using the predicate @pred{forgetting/7}.
").

%% ------------------------------------------------------------- %%
:- use_module('./forgetting_aux', _).
:- use_module('../../common_dependencies/graph/dual_rules_for_graphs', [generate_dual_for_predicate/4]).
:- use_module('../../common_dependencies/graph/graph', [find_all_exact_direct_dependents/4]).
:- use_module('../../common_dependencies/auxiliary_functions/aux_negation', [negate/3]).
:- use_module('../../common_dependencies/auxiliary_functions/auxiliary', [delete_first_occurrence/3, restore_facts_missing_predicates/2]).

%% ------------------------------------------------------------- %%



%!  forgetting(+PredicatesToForget:list, +PredicateList:list, +Program:list, +Loops:list, -NewPredicateList:lst, -NewProgram:list, +SCASPExec:int)
%   Given a list of the head of the predicates to forget (@var{PredicatesToForget}),
%   the program as a Branched Graph (@var{Program}) (see /dependencies/graph for more details),
%   the loops identified in the program generated with the module loops (@var{Loops}),
%   and list of the predicate heads generated while creating the graph (@var{PredicateList}),
%   this predicate returns a program, as a Branched graph, that will generate the same results without using the forgotten predicates @var{NewProgram}
%   and a list of the predicate heads it contains (@var{NewPredicateList}).


f_casp([Predicate|PredicatesToForget], Program, ForgottenProgram, SCASPExec) :-
    % Step 1: transform the clauses of the predicate to forget
    %% transform facts and missing predicates
    add_clauses_if_needed(Predicate, Program, Program1),
    %% transform even loops
    transform_even_loops(Predicate, Program1, Program2, Aux),
    %% delete predicate when it is present on its own body
    delete_auto_calls(Predicate, Program2, Program3),
    % Step 2: generate s(CASP) dual rules for predicate
    generate_dual_for_predicate(Predicate, Program3, DualRule, 0),
    % Step 3: forget a predicate and its negation
    forget_predicate(Predicate, DualRule, Program3, Program4),
    % Step 4: restore even loops, facts and missing predicates
    restore_even_loops(Program4, Aux, Program5),
    restore_facts_missing_predicates(Program5, Program6),
    % REPEAT 1,2,3: continue forgetting the rest of predicates before transforming the double negations
    f_casp(PredicatesToForget, Program6, ForgottenProgram, SCASPExec).

f_casp([], Program, Program_sCASP, 1):-
    % Step 5: transform double negations into even loops
    transform_double_negations(Program, Program_sCASP).

f_casp([], Program, Program, 0).

forget_predicate(Predicate, Dual, Program, NewProgram):-
    find_all_exact_direct_dependents(Predicate, Dependents, Program, 0),
    member([Predicate, Value], Program),
    Dual = [NegatedPredicate, ValueD],
    find_all_exact_direct_dependents(NegatedPredicate, DependentsD, Program, 0),
    replace_predicate(Predicate, Value, Dependents, Program, IntermediateProgram),
    replace_predicate(NegatedPredicate, ValueD, DependentsD, IntermediateProgram, NewProgram2),
    % delete predicate clauses
    select([Predicate, Value], NewProgram2, NewProgram).

restore_even_loops(Program, Aux, NewProgram):-
    find_all_exact_direct_dependents(Aux, Dependents, Program, 0),
    replace_predicate(Aux, not(not(Aux)), Dependents, Program, NewProgram).