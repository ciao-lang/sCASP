:- module('forgetting_aux', 
[
    transform_even_loops/4,
    add_clauses_if_needed/3,
    replace_predicate/5,
    transform_double_negations/2,
    replace_negation/4,
    delete_auto_calls/3,
    disappear_odd_loops/2
]).

%% ------------------------------------------------------------- %%
:- use_module('../../common_dependencies/graph/graph', [
    is_leaf/3, add_value_to_leaf/5,
    find_all_exact_direct_dependents/4, find_all_direct_dependencies/4
    ]).
:- use_module('../../common_dependencies/auxiliary_functions/auxiliary', [
    appear/2, combine/3, add_list_level/2, delete_all_occurrences/3,
    avoid_doble/2, avoid_triple/2, merge_lists/3, flatten_list/2,
    delete_in_lists_of_lists/3, destructive_append/3,
    before_after_element/4, delete_repeated_elements/2, double_list/1,
    replace_all_keeping_order/4, replace_in_lists/4
]).
:- use_module('../../common_dependencies/auxiliary_functions/aux_negation', [negate/3, negate_pred/2]).
:- use_module('../../../scasp_io', [next_counter/1]).
:- use_module('../../common_dependencies/loops/loops', [find_loops_predicate/4, filter_loops/3]).

%% ------------------------------------------------------------- %%

% +Predicate = Predicate to forget
% +Program = BranchGraph
% -NewProgram = Graph with predicate transformed
% -Aux = List of auxiliary predicates created

% Transform even loops
transform_even_loops(Predicate, Program, NewProgram, Aux):-
    negate_pred(Predicate, NegatedPredicate),
    (
        (
            find_all_exact_direct_dependents(NegatedPredicate, D, Program, 0),
            D \= []
        );(
            find_all_exact_direct_dependents(not(NegatedPredicate), D, Program, 0),
            D \= []
        )
    ),
    find_loops_predicate(Predicate, Program, 0, Loops),
    % 2 options:
    % 1 - Same behavior for even and odd loops
    Loops \= [],
    Loops \= [[Predicate]],
    Loops \= [[Predicate, Predicate]],
    !,
    ((
    % % 2 - Different behavior depending on the loops
    % filter_loops(Loops, EvenLoops, OddLoops),
    % !,
    % ((((
    %     % the predicate is part of an even loop
    %     EvenLoops \= [],
    %     EvenLoops \= [[Predicate]],
    %     EvenLoops \= [[Predicate, Predicate]]
    % );(
    %     % the predicate has a double negation
    %     find_all_exact_direct_dependents(not(not(Predicate)), D, Program, 0),
    %     D \= []
    % );(
    %     % the predicate is part of an odd loop that is going to disappear
    %     disappear_odd_loops(OddLoops, Predicate)
    % )),
    % % 
    % get auxiliar predicate and replace the negation with it
    get_auxiliar_predicate(Name),
    replace_negation(Predicate, Name, Program, NewProgram1), 
    destructive_append(NewProgram1,[[Name, [[NegatedPredicate]]]], NewProgram),
    Aux = Name
    );(
        NewProgram = Program,
        Aux = []
    )), !.


% the predicate appears as a double negation
transform_even_loops(Predicate, Program, NewProgram, Name):-
    negate_pred(Predicate, NegatedPredicate),
    find_all_exact_direct_dependents(not(not(Predicate)), D, Program, 0),
    D \= [],
    get_auxiliar_predicate(Name),
    replace_negation(Predicate, Name, Program, NewProgram1),
    destructive_append(NewProgram1,[[Name, [[NegatedPredicate]]]], NewProgram), !.

transform_even_loops(_, Program, Program, []).

loop_double_negation(Predicate, Program):-
    find_all_direct_dependencies(Predicate, Dependencies, Program, 0),
    flatten_list(Dependencies, FlattenDeps),
    member(not(not(Predicate)), FlattenDeps), !.

disappear_odd_loops([Loop|_OddLoops], Predicate):-
    disappear_odd_loop(Loop, Predicate).

disappear_odd_loops([_Loop|OddLoops], Predicate):-
    disappear_odd_loops(OddLoops, Predicate).

disappear_odd_loop(OddLoop, not(not(Predicate))):-
    delete_all_occurrences(not(not(Predicate)), OddLoop, OL1),
    delete_all_occurrences(not(Predicate), OL1, OL2),
    delete_all_occurrences(Predicate, OL2, Final),
    !,
    Final = [].

disappear_odd_loop(OddLoop, not(Predicate)):-
    delete_all_occurrences(not(not(Predicate)), OddLoop, OL1),
    delete_all_occurrences(not(Predicate), OL1, OL2),
    delete_all_occurrences(Predicate, OL2, Final),
    !, 
    Final = [].

disappear_odd_loop(OddLoop, Predicate):-
    delete_all_occurrences(not(not(Predicate)), OddLoop, OL1),
    delete_all_occurrences(not(Predicate), OL1, OL2),
    delete_all_occurrences(Predicate, OL2, Final),
    !,
    Final = [].

% Transform fact clauses in predicates with multiple clauses
add_clauses_if_needed(Predicate, Graph, NewGraph):-
    find_all_direct_dependencies(Predicate, Dependencies, Graph, 0),
    member([], Dependencies),
    replace_in_lists([], Dependencies, [[true]], NewDependencies),
    replace_all_keeping_order(Graph, [Predicate, Dependencies], [Predicate, NewDependencies], NewGraph), !.

add_clauses_if_needed(Predicate, Graph, NewGraph):-
    is_leaf(Predicate, Graph, 0),
    detect_value_for_leaf(Predicate, Graph, Value),
    add_value_to_leaf(Graph, Predicate, Value, NewGraph, 0), !.

%% Case p:- p.
add_clauses_if_needed(Predicate, Graph, NewGraph):-
    find_all_direct_dependencies(Predicate, Dependencies, Graph, 0),
    %negate_pred(Predicate, NegatedPredicate),
    flatten_list(Dependencies, Deps1),
    delete_all_occurrences(Predicate, Deps1, Deps3),
    %delete_all_occurrences(NegatedPredicate, Deps2, Deps3),
    (
        (Deps3 = [],
        delete_all_occurrences([Predicate, _], Graph, Graph1),
        append([[Predicate, [[false]]]], Graph1, NewGraph))
        ;
        NewGraph = Graph
    ), !.

%% Add "true" to empty clauses when the predicate has multiple clauses.
add_clauses_if_needed(Predicate, Graph, NewGraph):-
    find_all_direct_dependencies(Predicate, Dependencies, Graph, 0),
    member([], Dependencies),
    delete_all_occurrences([], Dependencies, Dependencies1),
    NewDependencies = [[true]|Dependencies1],
    delete_all_occurrences([Predicate, _], Graph, Graph1),
    append([[Predicate, NewDependencies]], Graph1, Graph2),
    add_clauses_if_needed(Predicate, Graph2, NewGraph), !.

add_clauses_if_needed(_, Graph, Graph).

% Case p :- r, p. / p:- r, not p. (Delete the predicate to forget from the body of its own clauses)
delete_auto_calls(Predicate, Program, NewProgram):-
    member([Predicate, Dependencies], Program),
    negate_pred(Predicate, NegatedPredicate),
    (
        (appear(Predicate, Dependencies),
        NewDependencies0 = [[true]|Dependencies])
    ;
        NewDependencies0 = Dependencies
    ),(
        (appear(NegatedPredicate, NewDependencies0),
        NewDependencies1 = [[false]|NewDependencies0])
    ;
        NewDependencies1 = NewDependencies0
    ),
    delete_in_lists_of_lists(Predicate, NewDependencies1, NewDependencies2),
    delete_in_lists_of_lists(Predicate, NewDependencies2, NewDependencies3),
    delete_all_occurrences([], NewDependencies3, NewDependencies),
    select([Predicate, Dependencies], Program, Program1),
    append([[Predicate, NewDependencies]], Program1, NewProgram), !.

delete_auto_calls(_Predicate, Graph, Graph).

% missing predicate
detect_value_for_leaf(Predicate, Graph, [[false]]):-
    \+member([Predicate|_], Graph),
    Predicate \= not(_), !.

% fact
detect_value_for_leaf(Predicate, _, [[true]]):-
    Predicate \= not(_), !.

% get the auxiliary predicate name using a pre-established counter
get_auxiliar_predicate(Aux):-
    next_counter(Number),
    atom_number(Atom, Number),
    atom_concat('neg_', Atom, Aux), !.

%!  replace_negation(+Predicate:atom, +NameOfAuxiliarPredicate:atom, +Program:list, -NewProgram:list)
%   Replace the negation of a \var{Predicate} to forget with an auxiliary predicate (@var{NameOfAuxiliarPredicate})
%   in a @var{Program}. Returns the new program in @var{NewProgram}.
replace_negation(_Predicate, _NameOfAuxiliarPredicate, [], []).

replace_negation(Predicate, NameOfAuxiliarPredicate, [Node|Graph], [NewNode|NewGraph]):-
    replace_negation_node(Predicate, NameOfAuxiliarPredicate, Node, NewNode),
    replace_negation(Predicate, NameOfAuxiliarPredicate, Graph, NewGraph), !.

replace_negation_node(_Predicate, _NameOfAuxiliarPredicate, [Head, []], [Head, []]).
replace_negation_node(Predicate, NameOfAuxiliarPredicate, [Head, [B1|Bs]], [Head, [NewB1|NewBs]]):-
    replace_negation_body(Predicate, NameOfAuxiliarPredicate, B1, NewB1),
    replace_negation_node(Predicate, NameOfAuxiliarPredicate, [Head, Bs], [Head, NewBs]), !.

replace_negation_body(_Predicate, _NameOfAuxiliarPredicate, [], []) :- !.
replace_negation_body(Predicate, NameOfAuxiliarPredicate, [P|Ps], [NewP|NewPs]):-
    P = not(Predicate), !,
    NewP = NameOfAuxiliarPredicate,
    replace_negation_body(Predicate, NameOfAuxiliarPredicate, Ps, NewPs).

replace_negation_body(Predicate, NameOfAuxiliarPredicate, [P|Ps], [NewP|NewPs]):-
    P = not(not(Predicate)), !,
    NewP = not(NameOfAuxiliarPredicate),
    replace_negation_body(Predicate, NameOfAuxiliarPredicate, Ps, NewPs).

replace_negation_body(Predicate, NameOfAuxiliarPredicate, [P|Ps], [P|NewPs]):-
    replace_negation_body(Predicate, NameOfAuxiliarPredicate, Ps, NewPs).

% Forget a predicate in a program (graph) indicating its dependents.
% (replace each appearance of Predicate for the predicates contained in NewNode)
replace_predicate([], _, _, Graph, Graph).
replace_predicate(_, _, [], Graph, Graph).
replace_predicate(Node, NewNode, [D|Dependents], Graph, NewGraph) :-
    before_after_element([D, Deps], PreviousGraph, Graph, AfterGraph),
    replace_and_permute(Node, Deps, NewNode, NewDeps1),
    add_list_level(NewDeps1, NewDeps),
    IntermediateGraph1 = [[D, NewDeps]|AfterGraph],
    destructive_append(PreviousGraph, IntermediateGraph1, IntermediateGraph),
    replace_predicate(Node, NewNode, Dependents, IntermediateGraph, NewGraph).

% Replace a predicate and make the permutation
% with the rest of predicates in the body.
replace_and_permute(_, [], _, []).

% Replace in a double list
replace_and_permute(Old, [[]|List], Content, NewList) :-
    replace_and_permute(Old, List, Content, NewList1),
    NewList = [[]|NewList1], !.

replace_and_permute(Old, [L|List], Content, NewList) :-
    double_list([L|List]),
    replace_and_permute(Old, L, Content, NewL),
    replace_and_permute(Old, List, Content, NewList2),
    ( ( NewL = [],
            NewL1 = NewL ) ;
        avoid_triple(NewL, NewL1) ),
    merge_lists(NewL1, NewList2, NewList), !.

% Replace in a simple list
replace_and_permute(Old, List, Content, NewList) :-
    member(Old, List),
    delete_all_occurrences(Old, List, OtherContents),
    avoid_doble(Content, Content1),
    combine(Content1, OtherContents, NewList1),
    avoid_doble(NewList1, NewList), !.

replace_and_permute(_, List, _, List).

% Transform double negations into even loops (step 4)

% Step 4 is executed
transform_double_negations(Program, NewProgram):-
    find_double_negations(Program, Doubles),
    replace_double_negations(Program, Doubles, NewProgram).

% Find all double negations
find_double_negations([],[]).
find_double_negations([Node|Graph], AllDoubles):-
    Node = [Predicate, Deps],
    find_double_negations_(Deps, Doubles),
    (
        (Predicate == not(not(_)),
        merge_lists(Doubles, [Predicate], Doubles1))
    ;
        Doubles1 = Doubles
    ),!,
    find_double_negations(Graph, OtherDoubles),
    merge_lists(Doubles1, OtherDoubles, AllDoubles), !.

find_double_negations_([], []).
find_double_negations_([Branch|Deps], Doubles):-
    find_double_negations_list(Branch, Doubles1),
    find_double_negations_(Deps, Doubles2),
    merge_lists(Doubles1, Doubles2, Doubles).

find_double_negations_list([], []).
find_double_negations_list([Element|List], Doubles):-
    Element = not(not(_)),
    find_double_negations_list(List, Doubles1),
    merge_lists([Element], Doubles1, Doubles), !.

find_double_negations_list([_|List], Doubles):-
    find_double_negations_list(List, Doubles).

% Replace double negations for even loops
replace_double_negations(Program, [], Program).
replace_double_negations(Program, [D|Doubles], NewProgram):-
    D = not(not(X)),
    find_all_exact_direct_dependents(D, Dependents, Program, 0),
    get_auxiliar_predicate(Name),
    replace_double_negations_deps(Program, D, Name, Dependents, NewProgram1),
    avoid_doble(X, X1),
    negate(X1, [], NegX),
    add_list_level(NegX, NegX1),
    append([[Name, NegX1]], NewProgram1, NewProgram2),
    replace_double_negations(NewProgram2, Doubles, NewProgram).

replace_double_negations_deps(Program, _Double, _NewAuxName, [], Program).
replace_double_negations_deps(Program, Double, NewAuxName, [Dependent|Dependents], NewProgram):-
    before_after_element([Dependent, Deps], PreviousProgram, Program, AfterProgram),
    replace_double_negations_clauses([], Double, NewAuxName, Deps, NewDeps),
    NewProgram2 = [[Dependent, NewDeps]|AfterProgram],
    destructive_append(PreviousProgram, NewProgram2, NewProgram1),
    replace_double_negations_deps(NewProgram1, Double, NewAuxName, Dependents, NewProgram).

replace_double_negations_clauses(Clauses, _Double, _NewAuxName, [], NewClauses):-
    reverse(Clauses, NewClauses).
replace_double_negations_clauses(Acc, Double, NewAuxName, [Clause|Clauses], NewClauses):-
    member(Double, Clause),
    before_after_element(Double, PreviousPreds, Clause, AfterPreds),
    NewDeps1 = [not(NewAuxName)|AfterPreds],
    destructive_append(PreviousPreds, NewDeps1, NewClause),
    NewClauses1 = [NewClause|Acc],
    replace_double_negations_clauses(NewClauses1, Double, NewAuxName, Clauses, NewClauses).

replace_double_negations_clauses(Acc, Double, NewAuxName, [Clause|Clauses], NewClauses):-
    replace_double_negations_clauses([Clause|Acc], Double, NewAuxName, Clauses, NewClauses).
