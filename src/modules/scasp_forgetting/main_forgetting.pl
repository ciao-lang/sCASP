:- module(main_forgetting, [
    main_forgetting/0
]).

%% ------------------------------------------------------------- %%
:- use_package(assertions).
%% ------------------------------------------------------------- %%
:- use_module(library(lists)).
:- use_module('./forgetting/forgetting').
:- use_module('../../scasp_io', [forgetting_parameters/1, init_counter/0]).
:- use_module('../common_dependencies/print', [print_dcg/1]).
:- use_module('../common_dependencies/parser', [program_to_graph/2]).

%% ------------------------------------------------------------- %%
:- doc(section, "Main predicates").

:- pred main_forgetting # "Makes preparations for forgetting, and executes f_casp/4 from module 'forgetting'.".

main_forgetting:-
    % init counter for auxiliar predicates
    init_counter,
    % obtain list of predicates to forget and value of SCASPExec from input
    forgetting_parameters([PredicatesToForget, SCASPExec]),
    % transform to graph
    program_to_graph(Graph, 0),
    % call f_casp/4
    f_casp(PredicatesToForget, Graph, ForgottenGraph, SCASPExec),
    % print result
    print_dcg(ForgottenGraph).

